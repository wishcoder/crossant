package us.wishcoder.crossant.compare;

import java.io.Serializable;

/**
* <h1>CompareData Bean</h1>
* <p>Used to serialize and deserialize 
* application state comparison results 
*
*
* @author  Ajay Singh
* @version 1.0
* @since   2016-01-01 
* 
* 
*/
public class CompareData implements Serializable {
	private static final long serialVersionUID = -8424042728481596065L;
	
	private final boolean ignore;
	private final String scenarioName;
	private final String className;
	private final String methodName;
	private final String dateSource;
	private final String dateCompare;
	
	/**
	 * @param ignore
	 * @param scenarioName
	 * @param className
	 * @param methodName
	 * @param dateSource
	 * @param dateCompare
	 */
	public CompareData(boolean ignore, String scenarioName, String className,
			String methodName, String dateSource, String dateCompare) {
		this.ignore = ignore;
		this.scenarioName = scenarioName;
		this.className = className;
		this.methodName = methodName;
		this.dateSource = dateSource;
		this.dateCompare = dateCompare;
	}

	/**
	 * This method is used to check if test scenario should be ignored
	 * 
	 * @return the ignore
	 */
	public boolean isIgnore() {
		return ignore;
	}

	/**
	 * Method to return scenario name.
	 * Scenario name is set in excel file scenario tab
	 * 
	 * @return the scenarioName
	 */
	public String getScenarioName() {
		return scenarioName;
	}

	/**
	 * This method is used to return JUnit test class name
	 * for test scenario
	 * 
	 * @return the className
	 */
	public String getClassName() {
		return className;
	}

	/**
	 * This method is used to return name of the method 
	 * in JUnit test class
	 * 
	 * @return the methodName
	 */
	public String getMethodName() {
		return methodName;
	}

	/**
	 * This method is used to return source date
	 * Source date is the date of one XML output file
	 * that contains application states 
	 * 
	 * @return the dateSource
	 */
	public String getDateSource() {
		return dateSource;
	}

	/**
	 * This method is used to return compare date
	 * Compare date is the date of other XML output file
	 * that contains application states
	 * 
	 * @return the dateCompare
	 */
	public String getDateCompare() {
		return dateCompare;
	}
}
