package us.wishcoder.crossant.compare;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.custommonkey.xmlunit.DetailedDiff;
import org.custommonkey.xmlunit.Diff;
import org.custommonkey.xmlunit.Difference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import us.wishcoder.crossant.annotation.CrossantStateWriter;
import us.wishcoder.crossant.launcher.CrossantEnvironment;
import us.wishcoder.crossant.util.DateUtils;
import us.wishcoder.crossant.util.ExcelUtil;
import us.wishcoder.crossant.util.TestRunnerUtil;

/**
* <h1>Compare application state results</h1> 
* <p>The UnitCompare program implements method to compare
* application state results between to result files 
*
* <h2>Steps to use UnitCompare:</h2>
* 
* <p><h3>1.</h3>Provide 'Compare' data inputs in TestSuite.xls and place
* file in [project]/src/main/resources/suites/suite1/ folder
* <code>
* ------------------------------------------------------------------------------------------------------------------------------------------
* <h4><u>Compare Data Inputs</u></h4>
* <h5>Ignore: FALSE</h5>
* <h5>Scenario: scenario-buy-sell</h5>
* <h5>Class: us.wishcoder.marketplace.client.tests.TestUser1Sell</h5>															
* <h5>Method: exportSellItemsState</h5>
* <h5>Date_Source: 2016-01-30</h5>
* <h5>Date_Compare: 2016-01-31</h5>
* ------------------------------------------------------------------------------------------------------------------------------------------
* <h3>2.</h3>Create a new Run or Debug configuration in Eclipse for your project
* <h3>3.</h3>Provide '-Dtestsuite=suite1 -XshowSettings:properties' in VM arguments in configuration
* <h3>4.</h3>Run us.wishcoder.crossant.compare.UnitCompare main class from application project
* <p>Compare results will be saved in [project]/results/suites/suite1/compare folder
* <code>
* 
* @author  Ajay Singh
* @version 1.0
* @since   2016-01-01 
* 
*/
public class UnitCompare {
	protected static final Logger LOG = LoggerFactory
			.getLogger(UnitCompare.class);

	private static final short CELL_SCENARIO_METHOD_FOREGROUND_COLOR_INDEX = HSSFColor.GREY_25_PERCENT.index;
	private static final short CELL_SCENARIO_METHOD_FONT_COLOR_INDEX = HSSFColor.BLACK.index;
	private static final short CELL_OBJECT_FOREGROUND_COLOR_INDEX =  HSSFColor.GREY_25_PERCENT.index;
	
	private static final int CELL_SCENARIO_METHOD_INDEX = 0;
	private static final int CELL_OBJECT_INDEX = 1;
	private static final int CELL_DIFFERENCE_INDEX = 1;
	
	
	private String testSuite;
	private String testSuitePath;
	private String testSuiteFilePath;

	/**
	 * 
	 */
	public UnitCompare() throws Exception {

		initEnvironment();
	}

	/**
	 * Initialize environment
	 * 
	 * @throws Exception
	 */
	private void initEnvironment() throws Exception {
		String baseClassPath = System.getProperty("user.dir") + File.separator
				+ "target" + File.separator + "classes";
		LOG.info("UnitCompare started in " + baseClassPath);

		// required testSuite
		this.testSuite = CrossantEnvironment.getTestSuiteProperty();
		if ("".equals(testSuite)) {
			throw new Exception("Required environment property '"
					+ CrossantEnvironment.SYSTEM_PROPERTY_TEST_SUITE_KEY
					+ "' is missing");
		}

		// required testSuitePath
		this.testSuitePath = CrossantEnvironment.getTestsuitePathProperty()
				+ File.separator + testSuite;
		Path filePath = Paths.get(baseClassPath + File.separator
				+ testSuitePath);
		if (!Files.exists(filePath, LinkOption.NOFOLLOW_LINKS)) {
			throw new Exception("Unable to initialize test suit path '"
					+ testSuitePath + "'");
		}

		// required testSuiteFilePath
		this.testSuiteFilePath = CrossantEnvironment.getTestsuitePathProperty()
				+ File.separator + testSuite + File.separator
				+ CrossantEnvironment.getTestsuiteFileProperty();
		filePath = Paths
				.get(baseClassPath + File.separator + testSuiteFilePath);
		if (!Files.exists(filePath, LinkOption.NOFOLLOW_LINKS)) {
			throw new Exception("Required test suit file '"
					+ (baseClassPath + File.separator + testSuiteFilePath)
					+ "' is missing");
		}
	}

	/**
	 * Start comparing
	 * 
	 * @throws Exception
	 */
	public void startCompare() throws Exception {
		Workbook testSuiteBook = null;
		FileOutputStream compareFileOutput = null;
		
		try {
			testSuiteBook = ExcelUtil
					.getExcelWorkbook(this.testSuiteFilePath);
			List<CompareData> compareList = ExcelUtil
					.getListToCompare(testSuiteBook);

			if(compareList == null || compareList.size() == 0){
				throw new Exception("Nothing to compare");
			}
			
			String resultPath = CrossantEnvironment.getResultPath() + File.separator + this.testSuite;
			String compareResultPath = CrossantEnvironment.getCompareResultPath(this.testSuite);
			File tmpFile = new File(compareResultPath);
			tmpFile.mkdir();
			tmpFile = new File(compareResultPath + File.separator + testSuite + "-compare-results-" + DateUtils.format(new Date(),DateUtils.CUSTOM_DATE_PATTERN) + ".xls");
			tmpFile.createNewFile();
			
			compareFileOutput = new FileOutputStream(tmpFile, false);
			HSSFWorkbook workbook = new HSSFWorkbook();
			int worksheetRowIndex = -1;
			
			String fileUserNamePortion = "";
			String fileNamePortion = "";
			File fileCompareTo = null;
			
			HSSFRow row = null; 
			HSSFCell cell = null;
			HSSFCellStyle cellStyle = null;
			HSSFFont font = null;
			
			for (CompareData CompareData : compareList) {

				Class<?> classz = Class.forName(CompareData.getClassName());
				Method crossantStateWriterMethod = TestRunnerUtil
						.findAnnotatedMethod(classz, CrossantStateWriter.class,
								CompareData.getMethodName());
				if (crossantStateWriterMethod != null) {
					try {
						// create worksheet for scenario
						worksheetRowIndex = -1;
						HSSFSheet worksheet = workbook.createSheet(CompareData.getScenarioName());
						
						
						worksheetRowIndex ++;
						row = worksheet.createRow((short) worksheetRowIndex);
						
						// strip packege detail from class name
						String justClassName = CompareData.getClassName().substring(CompareData.getClassName().lastIndexOf(".") + 1);
						
						// Create bold heading with scenario-method name
						cell = row.createCell(CELL_SCENARIO_METHOD_INDEX);
						cell.setCellValue(CompareData.getScenarioName() + " [ " + justClassName + "." +  CompareData.getMethodName() + "() ]");
						cellStyle = workbook.createCellStyle();
						cellStyle.setFillForegroundColor(CELL_SCENARIO_METHOD_FOREGROUND_COLOR_INDEX);
						cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
						cellStyle.setWrapText(false);
						font = workbook.createFont();//Create font
						font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);//Make font bold
						font.setColor(CELL_SCENARIO_METHOD_FONT_COLOR_INDEX);
						cellStyle.setFont(font);
						cell.setCellStyle(cellStyle);
						
						@SuppressWarnings("unchecked")
						Map<String, Object> objectMap = (Map<String, Object>) crossantStateWriterMethod
								.invoke(classz.newInstance());
						for (Map.Entry<String, Object> entry : objectMap
								.entrySet()) {
							
							// Create bold heading with object
							LOG.info("Comparing '" + entry.getKey() + "' in " + CompareData.getMethodName() + "()");
							worksheetRowIndex ++;
							row = worksheet.createRow((short) worksheetRowIndex);
							cell = row.createCell(CELL_OBJECT_INDEX);
							cellStyle = workbook.createCellStyle();
							//cellStyle.setWrapText(true);
							cellStyle.setFillForegroundColor(CELL_OBJECT_FOREGROUND_COLOR_INDEX);
							cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
							font = workbook.createFont();//Create font
							font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);//Make font bold
							cellStyle.setFont(font);
							cell.setCellStyle(cellStyle);
							cell.setCellValue(entry.getKey());
							
							// construct date source file portion name
							fileNamePortion = CompareData.getScenarioName() + "_" + CompareData.getMethodName() + "_" + entry.getKey() + "_" + CompareData.getDateSource() + ".xml";
							
							List<File> sourceList = getSourceFiles(fileNamePortion, resultPath);
							for(File sourceFile : sourceList){
								fileUserNamePortion = sourceFile.getName().substring(0, sourceFile.getName().toLowerCase().indexOf("_scenario"));  
										
								// construct date compare file portion name
								fileNamePortion =  fileUserNamePortion + "_" + CompareData.getScenarioName() + "_" + CompareData.getMethodName() + "_" + entry.getKey() + "_" + CompareData.getDateCompare() + ".xml";
								fileCompareTo = getCompareToFile(fileNamePortion, resultPath); 
								if(fileCompareTo != null && fileCompareTo.exists()){
									
									Diff diff = new Diff(new FileReader(sourceFile), new FileReader(fileCompareTo));
									
						            DetailedDiff detDiff = new DetailedDiff(diff);
						            List<?> differences = detDiff.getAllDifferences();
						            for (Object object : differences) {
						            	Difference difference = (Difference)object;
						            	
						            	// Create rows for differences
										worksheetRowIndex ++;
										row = worksheet.createRow((short) worksheetRowIndex);
										cell = row.createCell(CELL_DIFFERENCE_INDEX);
										cellStyle = workbook.createCellStyle();
										cellStyle.setWrapText(true);
										cellStyle = workbook.createCellStyle();
										cell.setCellStyle(cellStyle);
										cell.setCellValue(difference.toString());
						            }
								}
							}
						}
						
						 //Setup the Page margins - Left, Right, Top and Bottom
						worksheet.setMargin(Sheet.LeftMargin, 0.25);
						worksheet.setMargin(Sheet.RightMargin, 0.25);
						worksheet.setMargin(Sheet.TopMargin, 0.75);
						worksheet.setMargin(Sheet.BottomMargin, 0.75);
						worksheet.autoSizeColumn(CELL_SCENARIO_METHOD_INDEX);
						worksheet.setColumnWidth(CELL_OBJECT_INDEX, 10000);
					} catch (Exception e) {
						LOG.error(e.getMessage(), e);
					}finally{
						if(workbook != null){
							workbook.close();
						}
						workbook = null;
					}
				}
			}
			workbook.write(compareFileOutput);
			compareFileOutput.flush();

		} finally {
			if (testSuiteBook != null) {
				testSuiteBook.close();
			}
			testSuiteBook = null;
			
			if(compareFileOutput != null){
				compareFileOutput.close();
			}
			compareFileOutput = null; 
		}
		
		LOG.info("UnitCompare completed");
	}
	

	
	/**
	 * Get source file to compare
	 * 
	 * @param fileNamePortion
	 * @param resultPath
	 * @return List of File
	 * @throws Exception
	 */
	private List<File> getSourceFiles(String fileNamePortion, String resultPath) throws Exception{
		List<File> fileList = new ArrayList<File>();
		
		File directory = new File(resultPath);
		//get all the files from a directory
        File[] fList = directory.listFiles();
		
        for (File file : fList){
            if(file.getName().toLowerCase().endsWith(fileNamePortion.toLowerCase()) ){
            	fileList.add(file);
            }
        }
        
		return fileList;
	}
	
	/**
	 * Get compare to file
	 * 
	 * @param fileName
	 * @param resultPath
	 * @return File
	 * @throws Exception
	 */
	private File getCompareToFile(String fileName, String resultPath) throws Exception{
		return new File(resultPath + File.separator + fileName);
	}

	/**
	 * main 
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			UnitCompare UnitCompare = new UnitCompare();
			UnitCompare.startCompare();
		} catch (Exception e) {
			LOG.error("Error while performing test compare due to the exception: "
					+ e.getMessage(), e);
		}

	}

}
