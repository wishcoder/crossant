package us.wishcoder.crossant.util;

import java.io.File;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import junit.framework.TestSuite;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import us.wishcoder.crossant.annotation.*;
import us.wishcoder.crossant.netty.Notification;
import us.wishcoder.crossant.netty.Notification.Type;
import us.wishcoder.crossant.netty.ScenarioData.Test;
import us.wishcoder.crossant.netty.ScenarioData.Test.FailureResult;
import us.wishcoder.crossant.runner.UnitTestRunner;
import us.wishcoder.crossant.util.helper.BasicRunListener;
import us.wishcoder.crossant.util.helper.XmlRunListener;

/**
 * <h1>Utility to run JUnit test cases from scenario data sheet in .xlsx
 * file</h1>
 * 
 * @author Ajay Singh
 * @version 1.0
 * @since 2016-01-01
 */
public class TestRunnerUtil {

	protected static final Logger LOG = LoggerFactory.getLogger(TestRunnerUtil.class);

	/**
	 * Execute tests in integrated mode
	 * 
	 * @param sessionId
	 * @param testSuiteStr
	 * @param notification
	 * @return notification
	 * @throws Exception
	 */
	public static Notification executeIntegratedTests(String sessionId, String testSuiteStr, Notification notification)
			throws Exception {

		notification.setType(Type.TEST_FAIL); // set default notification type to FAIL

		Test test = notification.getScenarioData().getAndUpdateReadyTest();

		String testScenario = test.getParentScenarioSheet().getName();

		LOG.info(sessionId + ": About to run integrated test for: " + test.toString());

		UnitTestRunner testRunner = null;
		Class<? extends Object> classz = null;
		try {
			ClassLoader sysClassLoader = ClassLoader.getSystemClassLoader();
			if (Boolean.getBoolean("debug.mode")) {
				URL[] urls = ((URLClassLoader) sysClassLoader).getURLs();
				for (URL url : urls) {
					LOG.info(sessionId + " URL: " + url.getFile());
				}
			}

			classz = sysClassLoader.loadClass(test.getClassName());
			testRunner = new UnitTestRunner(testScenario, classz, test.getMethodName());
		} catch (Throwable e) {
			LOG.error(sessionId + ": Unable to initialize UnitTestRunner due to the exception: " + e.getMessage(), e);
			throw new Exception(e);
		}

		LOG.info(sessionId + " testClass: " + classz);
		LOG.info(sessionId + " testRunner: " + testRunner);

		Method crossantStateWriterMethod = findAnnotatedMethod(classz, CrossantStateWriter.class, test.getMethodName());
		LOG.info(sessionId + " stateWriter: " + crossantStateWriterMethod);

		if (crossantStateWriterMethod == null) { // run JUnit test
			try {
				JUnitCore jUnitCore = new JUnitCore();
				jUnitCore.addListener(new BasicRunListener());
				Result testResult = jUnitCore.run(testRunner);
				test.setTestResult(testResult);

				if (testResult != null) {
					LOG.info(sessionId + " executed unit test for " + test.toString());
					if (testResult.getFailureCount() == 0) {
						notification.setType(Type.TEST_PASS);
					} else {
						List<FailureResult> failures = new ArrayList();
						for (Failure testFailure : testResult.getFailures()) {
							FailureResult failure = new FailureResult(testFailure.getDescription().toString(),
									testFailure.getMessage(), testFailure.getTrace(), testResult.getRunTime());
							failures.add(failure);
							LOG.error(sessionId + " unit test failed: " + failure.toString());
						}
						test.setFailures(failures);
					}
				}
			} catch (Exception e) {
				LOG.error(sessionId + " " + test.toString() + " failed due to exception: " + e.getMessage(), e);
			}
		} else { // write states
			try {
				Map<String, Object> objectMap = (Map<String, Object>) crossantStateWriterMethod
						.invoke(classz.newInstance());
				for (Map.Entry<String, Object> entry : objectMap.entrySet()) {
					String objectKey = sessionId + "_" + testScenario + "_" + test.getMethodName() + "_"
							+ entry.getKey() + "_" + DateUtils.format(new Date(), DateUtils.CUSTOM_DATE_PATTERN);
					try {
						if (entry.getValue() != null) {
							LOG.info("Write object state for '" + objectKey + "' at " + testSuiteStr);
							ResultWriterUtil.writeObjectState(testSuiteStr, objectKey, entry.getValue());
						} else {
							LOG.error("Unable to write object state for '" + objectKey + "' at " + testSuiteStr
									+ " due to NULL value");
						}
					} catch (Exception e) {
						LOG.error("Unable to write object state for '" + objectKey + "'", e);
					}
				}
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
			}
		}

		return notification;
	}

	/**
	 * find annotated method
	 * 
	 * @param clazz
	 * @param annotationClass
	 * @param methodName
	 * @return Method
	 */
	public static Method findAnnotatedMethod(Class<?> clazz, Class<? extends Annotation> annotationClass,
			String methodName) {
		for (Method method : clazz.getMethods())
			if (method.isAnnotationPresent(annotationClass) && method.getName().equalsIgnoreCase(methodName))
				return (method);
		return (null);
	}

	/**
	 * Execute tests in stand alone mode
	 * 
	 * @param sessionId
	 * @param testSuitePath
	 * @param testSuiteStr
	 * @param testSuiteFilePath
	 * @throws Exception
	 */
	public static void executeStandaloneTests(String sessionId, String testSuitePath, String testSuiteStr,
			String testSuiteFilePath) throws Exception {

		LOG.info("Executing standalone tests for '" + sessionId + "' from '" + testSuiteFilePath + "'");

		Workbook workbook = null;
		InputStream inputStream = null;
		List<Class> testClassesList = new ArrayList<Class>();

		try {
			inputStream = TestRunnerUtil.class.getClassLoader().getResourceAsStream(testSuiteFilePath);

			workbook = new XSSFWorkbook(inputStream);
			int numberOfSheets = workbook.getNumberOfSheets();
			for (int sheetIndex = 0; sheetIndex < numberOfSheets; sheetIndex++) {
				Sheet currentSheet = workbook.getSheetAt(sheetIndex);
				if (currentSheet.getSheetName().equalsIgnoreCase(sessionId)) {
					Iterator<Row> iterator = currentSheet.iterator();

					while (iterator.hasNext()) {
						Row currentRow = iterator.next();
						if (ExcelUtil.isHeaderRow(workbook, currentRow, ExcelUtil.EXCEL_TESTS_KEY)) { // Header Row

							while (iterator.hasNext()) {
								// get next row that contains data
								currentRow = iterator.next();
								Cell currentCell = currentRow.getCell(0);
								if (currentCell != null && currentCell.getStringCellValue() != null
										&& !"".equals(currentCell.getStringCellValue().trim())) {

									String classToTestStr = currentCell.getStringCellValue();
									Class classToTest = null;
									try {
										classToTest = Class.forName(classToTestStr);
										testClassesList.add(classToTest);
									} catch (Exception e) {
										LOG.error("Class '" + classToTestStr + "' not found for '" + sessionId + "'",
												e);
									}
								}
							}
							break;
						}
					}
				}
			}

			if (testClassesList.size() == 0) {
				LOG.error("No test class found for '" + sessionId + "'");
			} else {
				Class[] testClasses = testClassesList.toArray(new Class[testClassesList.size()]);

				JUnitCore jUnitCore = new JUnitCore();
				jUnitCore.addListener(new XmlRunListener(testSuiteStr));
				jUnitCore.addListener(new BasicRunListener());

				TestSuite testSuite = new TestSuite(
						sessionId + "_" + DateUtils.format(new Date(), DateUtils.CUSTOM_DATE_PATTERN));
				for (Class testClass : testClasses) {
					testSuite.addTestSuite(testClass);
				}
				Result testResult = jUnitCore.run(testSuite);

				if (testResult != null) {
					if (testResult.getFailureCount() > 0) {
						for (Failure failure : testResult.getFailures()) {
							LOG.error("Test failed for '" + sessionId + "'. Error: " + failure.toString());
						}
					} else {
						LOG.info("Test passed for '" + sessionId + "'. Successful: " + testResult.wasSuccessful());
					}
				}
			}
		} finally {
			if (workbook != null) {
				workbook.close();
			}
			if (inputStream != null) {
				inputStream.close();
			}
		}

	}
}
