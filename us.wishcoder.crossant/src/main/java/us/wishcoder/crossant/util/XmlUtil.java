package us.wishcoder.crossant.util;

import java.io.BufferedReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

/**
 * <h1>Utility for common XML functions</h1>
 * 
 * @author Ajay Singh
 * @version 1.0
 * @since 2016-01-01
 */
public class XmlUtil {
	public static final String PACKEGE_PATH = System.getProperty("report.package.path", "us.wishcoder.crossant.report");
	
	public static final double ONE_SECOND = 1000.0;
	public static final String TESTSUITES = "testsuites";
	public static final String TESTSUITE = "testsuite";
	public static final String TESTCASE = "testcase";
	public static final String FAILURE = "failure";
	public static final String ATTR_NAME = "name";
	public static final String ATTR_TIME = "time";
	public static final String ATTR_ERRORS = "errors";
	public static final String ATTR_FAILURES = "failures";
	public static final String ATTR_TESTS = "tests";
	public static final String ATTR_TYPE = "type";
	public static final String ATTR_MESSAGE = "message";
	public static final String PROPERTIES = "properties";
	public static final String ATTR_CLASSNAME = "classname";
	public static final String TIMESTAMP = "timestamp";
	public static final String HOSTNAME = "hostname";

	private static final String[] DEFAULT_TRACE_FILTERS = new String[] {
			"junit.framework.TestCase",
			"junit.framework.TestResult",
			"junit.framework.TestSuite",
			"junit.framework.Assert.", // don't filter AssertionFailure
			"junit.swingui.TestRunner", "junit.awtui.TestRunner",
			"junit.textui.TestRunner", "java.lang.reflect.Method.invoke(",
			"sun.reflect.", "org.apache.tools.ant.",
			// JUnit 4 support:
			"org.junit.", "junit.framework.JUnit4TestAdapter",
			// See wrapListener for reason:
			"Caused by: java.lang.AssertionError", " more", };

	/**
	 * getDocumentBuilder
	 * 
	 * @return DocumentBuilder
	 */
	public static final DocumentBuilder getDocumentBuilder() {
		try {
			return DocumentBuilderFactory.newInstance().newDocumentBuilder();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * get the local hostname
	 * 
	 * @return the name of the local host, or "localhost" if we cannot work it
	 *         out
	 */
	public static final String getHostname() {
		try {
			return InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			return "localhost";
		}
	}

	/**
	 * Returns a filtered stack trace. This is ripped out of
	 * junit.runner.BaseTestRunner.
	 * 
	 * @param t
	 *            the exception to filter.
	 * @return the filtered stack trace.
	 */
	public static String getFilteredTrace(Throwable t) {
		return filterStack(getStackTrace(t));
	}	
	
	/**
	 * Convenient method to retrieve the full stacktrace from a given exception.
	 * 
	 * @param t
	 *            the exception to get the stacktrace from.
	 * @return the stacktrace from the given exception.
	 */
	private static String getStackTrace(Throwable t) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw, true);
		t.printStackTrace(pw);
		pw.flush();
		pw.close();
		return sw.toString();
	}
	
	/**
	 * Filters stack frames from internal JUnit and Ant classes
	 * 
	 * @param stack
	 *            the stack trace to filter.
	 * @return the filtered stack.
	 */
	public static String filterStack(String stack) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		StringReader sr = new StringReader(stack);
		BufferedReader br = new BufferedReader(sr);

		String line;
		try {
			while ((line = br.readLine()) != null) {
				if (!filterLine(line)) {
					pw.println(line);
				}
			}
		} catch (Exception e) {
			return stack; // return the stack unfiltered
		}
		return sw.toString();
	}

	/**
	 * filterLine
	 * 
	 * @param line
	 * @return
	 */
	private static boolean filterLine(String line) {
		for (int i = 0; i < DEFAULT_TRACE_FILTERS.length; i++) {
			if (line.indexOf(DEFAULT_TRACE_FILTERS[i]) != -1) {
				return true;
			}
		}
		return false;
	}
}
