package us.wishcoder.crossant.util;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.ProjectHelper;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import us.wishcoder.crossant.launcher.CrossantEnvironment;
import us.wishcoder.crossant.netty.Notification;
import us.wishcoder.crossant.netty.ScenarioData;
import us.wishcoder.crossant.netty.ScenarioData.ScenarioSheet;
import us.wishcoder.crossant.netty.ScenarioData.Test;
import us.wishcoder.crossant.netty.ScenarioData.Test.FailureResult;
import us.wishcoder.crossant.util.helper.DOMElementWriter;
import us.wishcoder.crossant.util.helper.XmlRunListener;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

/**
 * <h1>Write test results</h1>
 * 
 * @author Ajay Singh
 * @version 1.0
 * @since 2016-01-01
 */
public class ResultWriterUtil {
	protected static final Logger LOG = LoggerFactory
			.getLogger(ResultWriterUtil.class);

	/**
	 * Write integration test result to file
	 * 
	 * @param testSuiteStr
	 * @param notification
	 * @param logDateTime
	 * 
	 * @throws Exception
	 */
	public static File writeTestResult(String testSuiteStr, Notification notification, String logDateTime) throws Exception {

		BufferedOutputStream outputStream = null;
		Writer reportWriter = null;
		Document xmlDocument = null;
		Element elementTestSuites = null;

		ScenarioData scenarioData = notification.getScenarioData();
		File resultTargetPath = null;

		try {

			resultTargetPath = new File(CrossantEnvironment.getResultPath() + File.separator + testSuiteStr);
			resultTargetPath.mkdirs();
			// deleting existing results if exists
			try {
				LOG.info("Cleaning result target path " + resultTargetPath);
				FileUtils.cleanDirectory(resultTargetPath);
			}catch(Exception e) {
				// ignore
			}
			
			File target = new File(resultTargetPath.getAbsolutePath() + File.separator + "JUnit_Result.xml");
			target.createNewFile();
			LOG.info("Writing test results to: " + target);
			
			String reportPackageName = XmlUtil.PACKEGE_PATH + "." + testSuiteStr.trim().toLowerCase();
			try {
				outputStream = new BufferedOutputStream(new FileOutputStream(
						target, false));

				LOG.info("Writing test result for '"
						+ notification.getScenarioData().getUid() + "' in "
						+ target.getAbsolutePath());

				// Root element
				xmlDocument = XmlUtil.getDocumentBuilder().newDocument();
				elementTestSuites = xmlDocument.createElement(XmlUtil.TESTSUITES);
				elementTestSuites.setAttribute(XmlUtil.ATTR_NAME, reportPackageName);
				// JUnit4 does not seem to discern between failures and errors.
				elementTestSuites.setAttribute(XmlUtil.ATTR_ERRORS, "" + 0);
				elementTestSuites.setAttribute(XmlUtil.ATTR_FAILURES, ""
						+ scenarioData.getTestsFailureCount());
				elementTestSuites.setAttribute(XmlUtil.ATTR_TESTS, ""
						+ scenarioData.getTestsCount());
				elementTestSuites
						.setAttribute(
								XmlUtil.ATTR_TIME,
								""
										+ (scenarioData.getTestsRunTime() / XmlUtil.ONE_SECOND));
				elementTestSuites.setAttribute(XmlUtil.TIMESTAMP,
						scenarioData.getUid());
				elementTestSuites.setAttribute(XmlUtil.HOSTNAME,
						XmlUtil.getHostname());

				// Write TestSuite for all test cases
				String scenarioSheetName = "";
				try {
					Element elementTestSuite = null;
					List<ScenarioSheet> sheets = scenarioData.getScenarioSheets();
					for(ScenarioSheet sheet : sheets) {
						scenarioSheetName = sheet.getName();
						elementTestSuite = writeScenarioTestResult(resultTargetPath, xmlDocument, testSuiteStr, scenarioData, sheet);
						if(null != elementTestSuite) {
							elementTestSuites.appendChild(elementTestSuite);
						}
					}
					
				}catch(Exception e) {
					LOG.error("Unable to write test result for sheet '" + scenarioSheetName + "'", e);
				}
				

				Writer wri = new BufferedWriter(new OutputStreamWriter(
						outputStream, "UTF8"));
				wri.write("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n");
				(new DOMElementWriter()).write(elementTestSuites, wri, 0, "  ");
				wri.flush();

				
				LOG.info("JUnit xml report is generated in " + resultTargetPath.getAbsolutePath());
				return resultTargetPath;
			} catch (FileNotFoundException e) {
				throw new RuntimeException(e);
			}
		} finally {
			if (outputStream != null) {
				outputStream.close();
			}
			outputStream = null;
		}
	}

	private static Element writeScenarioTestResult(File resultTargetPath, Document xmlDocument, String testSuiteStr, ScenarioData scenarioData, ScenarioSheet scenarioSheet) throws Exception{
		File target = new File(resultTargetPath.getAbsolutePath() + File.separator + "JUnit_Result_" + scenarioSheet.getName() + ".xml");
		LOG.info("Writing " + scenarioSheet.getName() + " test result to " + target);
		
		BufferedOutputStream outputStream = null;
		Writer reportWriter = null;
		Element elementTestSuite = null;
		
		String scenarioSheetStr = scenarioSheet.getName().trim().toLowerCase();
		scenarioSheetStr = testSuiteStr.trim().toLowerCase() + "-" + scenarioSheetStr.substring("scenario-".length() + 1);
		String reportPackageName = XmlUtil.PACKEGE_PATH + "." + scenarioSheetStr;
		
		try {
			elementTestSuite = xmlDocument.createElement(XmlUtil.TESTSUITES);
			elementTestSuite.setAttribute(XmlUtil.ATTR_NAME, reportPackageName);
			// JUnit4 does not seem to discern between failures and errors.
			elementTestSuite.setAttribute(XmlUtil.ATTR_ERRORS, "" + 0);
			elementTestSuite.setAttribute(XmlUtil.ATTR_FAILURES, ""
					+ scenarioData.getTestsFailureCount(scenarioSheet.getName()));
			elementTestSuite.setAttribute(XmlUtil.ATTR_TESTS, ""
					+ scenarioData.getTestsCount(scenarioSheet.getName()));
			elementTestSuite
					.setAttribute(
							XmlUtil.ATTR_TIME,
							""
									+ (scenarioData.getTestsRunTime(scenarioSheet.getName()) / XmlUtil.ONE_SECOND));
			elementTestSuite.setAttribute(XmlUtil.TIMESTAMP,
					scenarioData.getUid());
			elementTestSuite.setAttribute(XmlUtil.HOSTNAME,
					XmlUtil.getHostname());
			
			// TestCase element
			Element elementTestCase = null;
			
			List<Test> testList = scenarioData.getTests();
			for (Test test : testList) {
				if(CrossantEnvironment.JUNIT_REPORT_IGNORE_TEST.equalsIgnoreCase(test.getMethodName())) {
					continue;
				}
				
				Result testResult = test.getTestResult();
				elementTestCase = xmlDocument.createElement(XmlUtil.TESTCASE);
				elementTestCase.setAttribute(XmlUtil.ATTR_NAME, getMethodName(test, scenarioSheet.getName()));
				if(test.getFailures() == null || test.getFailures().size() == 0) {
					elementTestCase.setAttribute(XmlUtil.ATTR_TIME, ""
							+ (testResult.getRunTime() / XmlUtil.ONE_SECOND));
				}

				// write testcase failure
				if (testResult.getFailureCount() > 0) {
					for (Failure failure : testResult.getFailures()) {
						Element failureNode = xmlDocument
								.createElement(XmlUtil.FAILURE);
						failureNode.setAttribute(XmlUtil.ATTR_MESSAGE,
								failure.getMessage());
						failureNode.setAttribute(
								XmlUtil.ATTR_TYPE,
								(test.getMethodName() + "("
										+ test.getClassName() + ")"));
						String strace = XmlUtil.getFilteredTrace(failure
								.getException());
						Text trace = xmlDocument.createTextNode(strace);
						failureNode.appendChild(trace);
						elementTestCase.appendChild(failureNode);
					}
				}else if(test.getFailures() != null) {
					int failureRuntime = 0;
					for(FailureResult failure : test.getFailures()) {
						failureRuntime += failure.getRunTime();
						
						Element failureNode = xmlDocument
								.createElement(XmlUtil.FAILURE);
						failureNode.setAttribute(XmlUtil.ATTR_MESSAGE,
								failure.getMessage());
						failureNode.setAttribute(
								XmlUtil.ATTR_TYPE,
								(test.getMethodName() + "("
										+ test.getClassName() + ")"));
						String strace = failure.getTrace();
						Text trace = xmlDocument.createTextNode(strace);
						failureNode.appendChild(trace);
						elementTestCase.appendChild(failureNode);
					}
					elementTestCase.setAttribute(XmlUtil.ATTR_TIME, ""
							+ (failureRuntime / XmlUtil.ONE_SECOND));
				}

				elementTestSuite.appendChild(elementTestCase);
			}
			
			reportWriter = new BufferedWriter(new OutputStreamWriter(
					outputStream, "UTF8"));
			reportWriter.write("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n");
			(new DOMElementWriter()).write(elementTestSuite, reportWriter, 0, "  ");
			reportWriter.flush();

			
			return (Element) elementTestSuite.cloneNode(true);

			
		}finally {
			if(null != reportWriter) {
				reportWriter.close();
			}
			if(null != outputStream) {
				outputStream.close();
			}
			
			reportWriter = null;
			outputStream = null;
		}
	}
	
	
	/**
	 * Return prefix with scenarion name and user sesion id
	 * 
	 * @param Test
	 * @return
	 */
	private static String getPrefix(Test Test) {
		return Test.getParentScenarioSheet().getName() + "_"
				+ Test.getUser() + "_";
	}
	
	/**
	 * 
	 * @param test
	 * @param scenarioSheetName
	 * @return
	 */
	private static String getMethodName(Test test, String scenarioSheetName) {
		return scenarioSheetName.trim().toLowerCase() + "_" + test.getUser().trim().toLowerCase() + "_" + test.getMethodName();
	}
	

	/**
	 * Write object state
	 * @param testSuiteStr
	 * @param objectKey
	 * @param object
	 */
	public static void writeObjectState(String testSuiteStr, String objectKey, Object object) {
		OutputStreamWriter writer = null;
		FileOutputStream outputStream = null;

		try {
			File target = new File(CrossantEnvironment.getResultPath() + File.separator + testSuiteStr);
			target.mkdirs();
			target = new File(CrossantEnvironment.getResultPath() + File.separator + testSuiteStr + File.separator + objectKey + ".xml");
			target.createNewFile();
			
			outputStream = new FileOutputStream(target, false);
			writer = new OutputStreamWriter(outputStream,
					Charset.forName("UTF-8"));
			XStream xstream = new XStream(new DomDriver());
			xstream.toXML(object, writer);
		} catch (Exception e) {
			LOG.error("" + e.getMessage(), e);
		} finally {
			try {
				if (writer != null) {
					writer.close();
				}
				if (outputStream != null) {
					outputStream.close();
				}
			} catch (Exception e) {
				LOG.error("" + e.getMessage(), e);
			}
		}

	}
	
	/**
	 * 
	 * @param resultTargetPath
	 * @param testSuite
	 * @throws Exception
	 */
	public static void generateHtmlReport(File resultTargetPath, String testSuite) throws Exception{
		File antPath = CrossantEnvironment.getAntPath();
		FileUtils.copyDirectory(antPath, resultTargetPath);
		
		File buildFile = new File(resultTargetPath + File.separator + "build.xml");
		Project antProject = new Project();
		antProject.setUserProperty("ant.file", buildFile.getAbsolutePath());
		antProject.setUserProperty("report.title", WordUtils.capitalize(testSuite));
		
		antProject.init();
		ProjectHelper projectHelper = ProjectHelper.getProjectHelper();
		antProject.addReference("ant.projectHelper", projectHelper);
		projectHelper.parse(antProject, buildFile);
		antProject.executeTarget(antProject.getDefaultTarget());
		LOG.info("JUnit html report is generated in " + resultTargetPath + File.separator + "unitTestReports");
	}

}
