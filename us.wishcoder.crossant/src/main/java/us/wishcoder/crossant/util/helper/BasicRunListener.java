package us.wishcoder.crossant.util.helper;


import org.junit.runner.Description;
import org.junit.runner.Result;
import org.junit.runner.notification.RunListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * <h1>Reports only the test summary to stdout</h1>
 * 
 * @author Ajay Singh
 * @version 1.0
 * @since 2016-01-01
 * 
 *
 */
public class BasicRunListener extends RunListener {
	protected static final Logger LOG = LoggerFactory
			.getLogger(BasicRunListener.class);
	
	@Override
	public void testRunStarted(Description description) throws Exception {
		LOG.info("Starting test run: " + description.getDisplayName()
				+ ", " + description.testCount() + " tests");
	}

	@Override
	public void testRunFinished(Result result) throws Exception {
		float elapsed = Math.round(result.getRunTime() / 100000) * 100;

		LOG.info("Finished test run: " + result.getFailureCount()
				+ " failures, " + result.getIgnoreCount() + " ignored, "
				+ (result.getRunCount() + result.getIgnoreCount()) + " total. "
				+ "Elapsed: " + elapsed + " seconds");
	}
}