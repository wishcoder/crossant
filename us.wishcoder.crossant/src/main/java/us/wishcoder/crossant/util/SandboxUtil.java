package us.wishcoder.crossant.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.tools.ant.DefaultLogger;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.taskdefs.Java;
import org.apache.tools.ant.types.Path;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import us.wishcoder.crossant.launcher.CrossantClientLauncher;
import us.wishcoder.crossant.launcher.CrossantEnvironment;
import us.wishcoder.crossant.launcher.CrossantServerLauncher;

/**
 * <h1>Launch forked JVM using ANT API</h1>
 * 
 * @author Ajay Singh
 * @version 1.0
 * @since 2016-01-01
 */
public class SandboxUtil {
	private static final ExecutorService executor = Executors
			.newFixedThreadPool(5);

	protected static final Logger LOG = LoggerFactory
			.getLogger(SandboxUtil.class);
	
	/**
	 * Launch sand box
	 * 
	 * @param sessionId
	 * @param CrossantEnvironment
	 * @throws Exception
	 */
	public static void launchSandbox(String sessionId,
			CrossantEnvironment crossantUnitEnvironment) throws Exception {
		
		executor.execute(new AsyncSandboxProcessor(sessionId, crossantUnitEnvironment));

		// executor.s
		// executor.shutdown();
		// try {
		// executor.awaitTermination(100, TimeUnit.SECONDS);
		// } catch (InterruptedException ex) {
		// System.out.println("Interrupted...");
		// }
	}

	/**
	 * Shutdown all sandboxes
	 * 
	 * @throws Exception
	 */
	public static void shutdownAll() throws Exception {
		executor.shutdown();
	}
}

/**
 * Thread to load application in a sandbox
 * 
 * @author ajsingh
 * 
 */
class AsyncSandboxProcessor implements Runnable {
	protected static final Logger LOG = LoggerFactory
			.getLogger(AsyncSandboxProcessor.class);

	private String sessionId;
	private CrossantEnvironment crossantUnitEnvironment;

	/**
	 * @param sessionId
	 * @param CrossantEnvironment
	 */
	public AsyncSandboxProcessor(String sessionId,
			CrossantEnvironment crossantUnitEnvironment) {
		this.sessionId = sessionId;
		this.crossantUnitEnvironment = crossantUnitEnvironment;
	}

	public void run() {
		LOG.info("\n\n>>>>>>>>>>>> Loading session '" + sessionId
				+ "' in test suite - " + crossantUnitEnvironment.getTestSuite()
				+ "\n");

		Throwable caught = null;
		Project project = null;
		
		Java javaTask = null;
		File javaTaskOutputFile = null;
		FileOutputStream javaTaskOutputFileStream = null;
		File javaTaskErrorFile = null;
		FileOutputStream javaTaskErrorFileStream = null;
		File javaTaskLogDir = null;
		boolean dirCreated = false;
		
		try {
			String userDir = System.getProperty("user.dir");
		
			project = new Project();
			DefaultLogger logger = new DefaultLogger();
			project.addBuildListener(logger);
			project.setBaseDir(new File(userDir));
			
			String javaTaskLogDirPath = userDir + File.separator + CrossantServerLauncher.LOG_FOLDER + File.separator 
					+ CrossantEnvironment.getTestSuiteProperty() + File.separator + sessionId;
			
			
			javaTaskLogDir = new File(javaTaskLogDirPath);
			dirCreated = javaTaskLogDir.mkdirs();
			
			if(dirCreated) {
				final OutputStream stdOut = System.out;
				final OutputStream stdError = System.err;
				
				String timeStamp = DateUtils.format(new Date(), DateUtils.LOGS_DATETIME_PATTERN);
				
				// Set Error to File and Console
				javaTaskErrorFile = new File(javaTaskLogDirPath + File.separator + "JavaTask_Error_" + timeStamp);
				javaTaskErrorFile.createNewFile();
				LOG.info(sessionId + " java task error will be written to: " + javaTaskErrorFile);
				javaTaskErrorFileStream = new FileOutputStream(javaTaskErrorFile);
				final PrintStream javaTaskErrorPrintStream = new PrintStream(javaTaskErrorFileStream);
				logger.setErrorPrintStream(new PrintStream(new OutputStream() {
						@Override
						public void write(byte buf[], int off, int len) throws IOException {
							javaTaskErrorPrintStream.write(buf, off, len);
							try {
								stdError.write(buf, off, len);
							}catch(Exception e) {
								LOG.error(e.getMessage(), e);
							}
						}
					
						@Override
						public void write(int b) throws IOException {
							javaTaskErrorPrintStream.write(b);
							stdError.write(b);
						}
						
						@Override
						public void flush() throws IOException {
							super.flush();
							javaTaskErrorPrintStream.flush();
							stdError.flush();
						}
						
						@Override
						public void close() throws IOException {
							super.close();
							javaTaskErrorPrintStream.close();
							// stdError.close(); // Normally not done
						}
					}
				));
				
				// Set Output to File and Console
				javaTaskOutputFile = new File(javaTaskLogDirPath + File.separator + "JavaTask_Output_" + timeStamp);
				javaTaskOutputFile.createNewFile();
				LOG.info(sessionId + " java task output will be written to: " + javaTaskOutputFile);
				javaTaskOutputFileStream = new FileOutputStream(javaTaskOutputFile);
				final PrintStream javaTaskOutputPrintStream = new PrintStream(javaTaskOutputFileStream);
				logger.setOutputPrintStream(new PrintStream(new OutputStream() {
						@Override
						public void write(byte buf[], int off, int len) throws IOException {
							javaTaskOutputPrintStream.write(buf, off, len);
							try {
								stdOut.write(buf, off, len);
							}catch(Exception e) {
								LOG.error(e.getMessage(), e);
							}
						}
					
						@Override
						public void write(int b) throws IOException {
							javaTaskOutputPrintStream.write(b);
							stdOut.write(b);
						}
						
						@Override
						public void flush() throws IOException {
							super.flush();
							javaTaskOutputPrintStream.flush();
							stdOut.flush();
						}
						
						@Override
						public void close() throws IOException {
							super.close();
							javaTaskOutputPrintStream.close();
							// stdOut.close(); // Normally not done
						}
					}
				));
				
			}else {
				logger.setOutputPrintStream(System.out);
				logger.setErrorPrintStream(System.err);
			}
			
			if(Boolean.getBoolean("debug.mode")) {
				logger.setMessageOutputLevel(Project.MSG_DEBUG);
			}else {
				logger.setMessageOutputLevel(Project.MSG_INFO);
			}
				
			project.init();
			project.fireBuildStarted();

			/** initialize an java task **/
			javaTask = new Java();
			javaTask.setNewenvironment(true);
			javaTask.setTaskName(sessionId);
			javaTask.setProject(project);
			javaTask.setFork(true);
			javaTask.setFailonerror(true);
			javaTask.setClassname(CrossantClientLauncher.class.getName());
			//javaTask.setJvmargs(ExcelUtil.getJvmArgs(crossantUnitEnvironment, sessionId));
			List<String> jvmArgs = ExcelUtil.getJvmArgs(crossantUnitEnvironment, sessionId);
			
			String classPathPartStr = "";
			for(String jvmArg : jvmArgs){
				if(jvmArg.startsWith("-classpath")) {
					classPathPartStr = jvmArg.substring("-classpath".length() + 1);
					String classPathStr = "-classpath \"" + classPathPartStr + "\"";
					javaTask.createJvmarg().setLine(classPathStr);
				} else if(jvmArg.startsWith("-Djava.library.path=")) {
					javaTask.createJvmarg().setLine("-Djava.library.path=\"" + jvmArg.substring("-Djava.library.path=".length()) + "\"" );
				} else {
					javaTask.createJvmarg().setLine(jvmArg);
				}
				
				if(Boolean.getBoolean("debug.mode")) {
					LOG.info(sessionId + ".jvmArg: " + jvmArg);
				}
			}
			
			if(Boolean.getBoolean("debug.mode")) {
				javaTask.createJvmarg().setLine("-verbose:class");
			}
			
			javaTask.createArg().setValue(
					crossantUnitEnvironment.getBaseClassPath());
			javaTask.createArg().setValue(
					crossantUnitEnvironment.getTestSuitePath());
			javaTask.createArg().setValue(
					crossantUnitEnvironment.getTestSuite());
			javaTask.createArg().setValue(
					crossantUnitEnvironment.getTestSuiteFilePath());
			javaTask.createArg().setValue(sessionId);

			/** set the class path */
			Path classPath = new Path(project, classPathPartStr);
			//File classDir = new File(crossantUnitEnvironment.getBaseClassPath());
			javaTask.setClasspath(classPath);

			LOG.info(sessionId + " java task command line: " + javaTask.getCommandLine().toString());
			
			javaTask.setOutput(javaTaskOutputFile);
			javaTask.setError(javaTaskErrorFile);
			javaTask.setFailonerror(true);
			javaTask.setFork(true);
			
			javaTask.init();
			int ret = javaTask.executeJava();
			LOG.info(sessionId + " java task return code: " + ret);

		} catch (Exception e) {
			LOG.error(sessionId + " java task caught exception: " + e.getMessage(), e);
			caught = e;
		} finally {
			project.fireBuildFinished(caught);
		}
	}
}