package us.wishcoder.crossant.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;

import org.junit.runner.notification.RunListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * <h1>Loads system properties</h1>
 * 
 * @author Ajay Singh
 * @taskpackage org.junit.junit4
 * @version 1.0
 * @since 2016-01-01
 */
public class ConfigUtil {
	protected static final Logger LOG = LoggerFactory
			.getLogger(ConfigUtil.class);

	private static final Properties originalProperties = System.getProperties();
	public static Properties getOriginalProperties(){
		return (Properties) originalProperties.clone();
	}
	
	/**
	 * Load system properties from configuration file
	 * @param configFile
	 * @throws IOException
	 */
	public static void loadConfig(String configFile) throws IOException {
	

		Properties props = System.getProperties();

		// copy loaded properties to System properties
		// Don't override values that were explicitly set in the system
		// properties
		Properties sysProps = System.getProperties();
		for (Map.Entry<Object, Object> objectObjectEntry : props.entrySet()) {
			Map.Entry e = (Map.Entry) objectObjectEntry;
			if (!sysProps.containsKey(e.getKey())) {
				sysProps.put(e.getKey(), e.getValue());
			}
		}
	}
	
	/**
	 * loadSessionConfig
	 * @param config
	 */
	public static void loadSessionConfig(String config){
		Properties sysProps = getOriginalProperties();
		String[] rows = config.split(","); 
		for(String row : rows){
			String[] cols = row.split("=");
			if (!sysProps.containsKey(cols[0].trim())) {
				sysProps.put(cols[0].trim(), cols[1].trim());
			}
		}
		
		System.setProperties(sysProps);
	}

	/**
	 * readConfigFile
	 * @param configFile
	 * @return
	 * @throws IOException
	 */
	private static InputStream readConfigFile(String configFile)
			throws IOException {
		InputStream in = ConfigUtil.class.getClassLoader().getResourceAsStream(
				configFile);

		try {
			if (in == null)
				in = new FileInputStream(configFile);
		} catch (IOException ioe) {
			if (!configFile.equalsIgnoreCase(configFile)) {
				createSiteConfigFile(configFile);
				in = new FileInputStream(configFile);
			} else
				throw new IOException(ioe.getMessage());
		}

		return in;
	}

	/**
	 * createSiteConfigFile
	 * @param configFile
	 * @throws IOException
	 */
	private static void createSiteConfigFile(String configFile)
			throws IOException {
		File file = new File(configFile);
		if (!file.exists()) {
			file.createNewFile();
		}
	}
}
