/**
 * 
 */
package us.wishcoder.crossant.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import us.wishcoder.crossant.compare.CompareData;
import us.wishcoder.crossant.launcher.CrossantEnvironment;
import us.wishcoder.crossant.launcher.CrossantServerLauncher;
import us.wishcoder.crossant.netty.ScenarioData;
import us.wishcoder.crossant.netty.ScenarioData.ScenarioSheet;
import us.wishcoder.crossant.netty.ScenarioData.Test;

/**
 * <h1>Utility class for common excel functions</h1>
 * 
 * @author Ajay Singh
 * @version 1.0
 * @since 2016-01-01
 */
public class ExcelUtil {
	
	protected static final Logger LOG = LoggerFactory
			.getLogger(ExcelUtil.class);
	
	public static final String EXCEL_JVMARGS_KEY = "Jvmargs";
	public static final String EXCEL_TESTS_KEY = "Tests";

	public static final String EXCEL_SCENARIO_SHEET_KEY = "Scenario";
	public static final int EXCEL_SCENARIO_DESCRIPTION_ROW = 0;
	public static final int EXCEL_SCENARIO_DESCRIPTION_COL = 1;
	public static final int EXCEL_SCENARIO_IGNORE_ROW = 1;
	public static final int EXCEL_SCENARIO_IGNORE_COL = 1;
	public static final int EXCEL_SCENARIO_FAIL_FAST_ROW = 2;
	public static final int EXCEL_SCENARIO_FAIL_FAST_COL = 1;

	public static final String EXCEL_SCENARIO_TIMELINE_KEY = "Timeline";
	public static final int EXCEL_SCENARIO_TIMELINE_COL = 0;
	public static final int EXCEL_SCENARIO_USER_COL = 1;
	public static final int EXCEL_SCENARIO_CLASS_COL = 2;
	public static final int EXCEL_SCENARIO_METHOD_COL = 3;

	public static final String EXCEL_COMPARE_SHEET_KEY = "Compare";
	public static final int EXCEL_COMPARE_IGNORE_COL = 0;
	public static final int EXCEL_COMPARE_SCENARIO_COL = 1;
	public static final int EXCEL_COMPARE_CLASS_COL = 2;
	public static final int EXCEL_COMPARE_METHOD_COL = 3;
	public static final int EXCEL_COMPARE_DATE_SOURCE_COL = 4;
	public static final int EXCEL_COMPARE_DATE_COMPARE_COL = 5;

	/**
	 * 
	 * @param excelFile
	 * @return Workbook
	 * @throws Exception
	 */
	public static final Workbook getExcelWorkbook(final String excelFile)
			throws Exception {
		return getExcelWorkbook(excelFile, "");
	}

	/**
	 * 
	 * @param excelFile
	 * @param filePath
	 * @return Workbook
	 * @throws Exception
	 */
	public static final Workbook getExcelWorkbook(final String excelFile,
			final String filePath) throws Exception {
		String excelFilePath = excelFile;
		if (filePath != null && !"".equals(filePath.trim())) {
			excelFilePath = File.separator + filePath + File.separator
					+ excelFile;
		}
		
		LOG.info("excelFilePath: " + excelFilePath);

		InputStream inputStream = null;
		XSSFWorkbook workbook = null;
		try {
//			inputStream = ExcelUtil.class.getClassLoader().getResourceAsStream(
//					excelFilePath);
//			
//			ClassLoader sysClassLoader = ClassLoader.getSystemClassLoader();
			
			inputStream = new FileInputStream(excelFilePath);
			workbook = new XSSFWorkbook(inputStream);
		} finally {
			if (inputStream != null) {
				inputStream.close();
			}
			inputStream = null;
		}
		return workbook;
	}

	/**
	 * getJvmArgs
	 * 
	 * @param crossantUnitEnvironment
	 * @param sessionId
	 * 
	 * @return List of jvm arguments
	 * @throws Exception
	 */
	public static List<String> getJvmArgs(
			CrossantEnvironment crossantUnitEnvironment, final String sessionId)
			throws Exception {
		List<String> jvmArgsList = new ArrayList<String>();
		Workbook workbook = null;

		try {
			workbook = getExcelWorkbook(crossantUnitEnvironment
					.getTestSuiteFilePath());
			int numberOfSheets = workbook.getNumberOfSheets();

			Sheet currentSheet = null;
			for (int sheetIndex = 0; sheetIndex < numberOfSheets; sheetIndex++) {
				currentSheet = workbook.getSheetAt(sheetIndex);
				if (currentSheet.getSheetName().equalsIgnoreCase(sessionId)) {
					break;
				}
			}

			if (currentSheet != null) {
				Iterator<Row> iterator = currentSheet.iterator();
				Row currentRow = null;
				Cell currentCell = null;

				while (iterator.hasNext()) {
					currentRow = iterator.next();
					currentCell = currentRow.getCell(0);

					if (currentCell != null
							&& currentCell.getStringCellValue() != null) {

						if (currentCell.getStringCellValue().equalsIgnoreCase(
								EXCEL_TESTS_KEY)) {
							// Test entries started. break now.
							break;
						}

						if (currentCell.getStringCellValue().equalsIgnoreCase(
								EXCEL_JVMARGS_KEY)) {
							// 'Jvmargs' header found.
							// Go to next row to start reading JVM arg entries
							currentRow = iterator.next();
						}

						currentCell = currentRow.getCell(0);
						if (currentCell != null) {
							if (currentCell.getStringCellValue() != null) {
								// put jvm arguments in List
								jvmArgsList.add(currentCell
										.getStringCellValue().trim()
										.replaceAll("\"", ""));
							}
						}
					}
				}
			}
		} finally {
			if (workbook != null) {
				workbook.close();
			}
		}

		return jvmArgsList;
	}

	/**
	 * Check if row is header row
	 * 
	 * @param workbook
	 * @param row
	 * @return
	 */
	public static boolean isHeaderRow(Workbook workbook, Row row, String key) {
		Cell cell = row.getCell(0);
		CellStyle style = cell.getCellStyle();
		short fontIndex = style.getFontIndex();
		Font font = workbook.getFontAt(fontIndex);
		if (font.getBold()) {
			if (cell.getStringCellValue().equalsIgnoreCase(key)) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	/**
	 * Scenario count in workbook
	 * 
	 * @param workbook
	 * @return int
	 */
	public static int getScenarioCount(Workbook workbook) {
		int scenarioCount = 0;

		int numberOfSheets = workbook.getNumberOfSheets();
		boolean scenarioCountStarted = false;
		for (int sheetIndex = 0; sheetIndex < numberOfSheets; sheetIndex++) {
			Sheet currentSheet = workbook.getSheetAt(sheetIndex);
			if (!currentSheet
					.getSheetName()
					.toLowerCase()
					.startsWith(
							ExcelUtil.EXCEL_SCENARIO_SHEET_KEY.toLowerCase())) {
				if(!scenarioCountStarted) {
					continue;
				}else {
					break;
				}
			} else {
				scenarioCountStarted = true;
				scenarioCount++;
			}
		}

		return scenarioCount;
	}

	/**
	 * Active session count in workbook
	 * 
	 * @param workbook
	 * @return int
	 */
	public static int getSessionCount(Workbook workbook) {
		int sessionCount = 0;

		int numberOfSheets = workbook.getNumberOfSheets();
		boolean sessionCountStarted = false;
		
		for (int sheetIndex = 0; sheetIndex < numberOfSheets; sheetIndex++) {
			Sheet currentSheet = workbook.getSheetAt(sheetIndex);
			if (!currentSheet
					.getSheetName()
					.toLowerCase()
					.startsWith(
							ExcelUtil.EXCEL_SCENARIO_SHEET_KEY.toLowerCase())) {
				sessionCountStarted = true;
				sessionCount++;
			} else {
				if(!sessionCountStarted) {
					continue;
				}else {
					break;
				}
			}
		}

		return sessionCount;
	}
	
	/**
	 * Get ScenarioData from test suite excel workbook
	 * 
	 * @param workbook
	 * @param integratedTestUID
	 * @return ScenarioData
	 */
	public static ScenarioData getScenarioData(Workbook workbook,
			String integratedTestUID) {
		ScenarioData scenarioData = null;

		List<ScenarioSheet> scenarioSheets = new ArrayList<ScenarioData.ScenarioSheet>();

		int numberOfSheets = workbook.getNumberOfSheets();

		boolean scenarioCountStarted = false;
		
		for (int sheetIndex = 0; sheetIndex < numberOfSheets; sheetIndex++) {
			Sheet currentSheet = workbook.getSheetAt(sheetIndex);
			if (!currentSheet
					.getSheetName()
					.toLowerCase()
					.startsWith(
							ExcelUtil.EXCEL_SCENARIO_SHEET_KEY.toLowerCase())) {
				if(!scenarioCountStarted) {
					continue;
				}else {
					break;
				}
			}else {
				scenarioCountStarted = true;
				// initialize scenario sheet
				ScenarioSheet scenarioSheet = getScenarioSheet(currentSheet);
				if (scenarioSheet != null) {
					scenarioSheets.add(scenarioSheet);
				}
			}

		}

		if (scenarioSheets.size() > 0) {
			scenarioData = new ScenarioData(integratedTestUID, scenarioSheets);
		}

		return scenarioData;
	}

	/**
	 * Initialize ScenarioSheet from current sheet
	 * 
	 * @param currentSheet
	 * @return ScenarioSheet
	 */
	public static ScenarioSheet getScenarioSheet(Sheet currentSheet) {
		ScenarioSheet ScenarioSheet = null;

		// read 'ignore'
		boolean ignore = currentSheet.getRow(EXCEL_SCENARIO_IGNORE_ROW)
				.getCell(EXCEL_SCENARIO_IGNORE_COL).getBooleanCellValue();
		if (!ignore) {
			// read 'description'
			String description = currentSheet
					.getRow(EXCEL_SCENARIO_DESCRIPTION_ROW)
					.getCell(EXCEL_SCENARIO_DESCRIPTION_COL)
					.getStringCellValue();
			// read 'failFast'
			boolean failFast = currentSheet
					.getRow(EXCEL_SCENARIO_FAIL_FAST_ROW)
					.getCell(EXCEL_SCENARIO_FAIL_FAST_COL)
					.getBooleanCellValue();

			List<Test> Tests = new ArrayList<ScenarioData.Test>();

			// now read test details
			boolean timelineStarted = false;
			Iterator<Row> iterator = currentSheet.iterator();
			while (iterator.hasNext()) {
				Row currentRow = iterator.next();

				if (!timelineStarted) {
					Cell currentCell = currentRow.getCell(0);

					if (currentCell.getStringCellValue().equalsIgnoreCase(
							EXCEL_SCENARIO_TIMELINE_KEY)) {
						timelineStarted = true;
						currentRow = iterator.next(); // go to next row to
														// read test
														// details
					}
				}

				if (!timelineStarted) {
					continue;
				} else {
					// read 'delay'
					double delay = 0;
					if (currentRow.getCell(EXCEL_SCENARIO_TIMELINE_COL) == null) {
						break;
					} else {
						delay = currentRow.getCell(EXCEL_SCENARIO_TIMELINE_COL)
								.getNumericCellValue();
					}

					// read 'user'
					String user = "";
					if (currentRow.getCell(EXCEL_SCENARIO_USER_COL)
							.getStringCellValue() == null) {
						break;
					} else {
						user = currentRow.getCell(EXCEL_SCENARIO_USER_COL)
								.getStringCellValue();
					}

					// read 'className'
					String className = "";
					if (currentRow.getCell(EXCEL_SCENARIO_CLASS_COL)
							.getStringCellValue() == null) {
						break;
					} else {
						className = currentRow
								.getCell(EXCEL_SCENARIO_CLASS_COL)
								.getStringCellValue();
					}

					// read 'methodName'
					String methodName = "";
					if (currentRow.getCell(EXCEL_SCENARIO_METHOD_COL)
							.getStringCellValue() == null) {
						break;
					} else {
						methodName = currentRow.getCell(
								EXCEL_SCENARIO_METHOD_COL).getStringCellValue();
					}

					Test Test = new ScenarioData.Test(delay, user, className,
							methodName, ScenarioSheet);
					Tests.add(Test);
				}
			}

			if (Tests.size() > 0) {
				ScenarioSheet = new ScenarioSheet(currentSheet.getSheetName(),
						description, ignore, failFast, Tests);
			}
		}

		return ScenarioSheet;
	}

	/**
	 * Return list of CompareData
	 * 
	 * @param workbook
	 * @return List of CompareData
	 */

	public static List<CompareData> getListToCompare(Workbook workbook)
			throws Exception {
		List<CompareData> compareList = new ArrayList<CompareData>();
		Sheet compareSheet = null;

		int numberOfSheets = workbook.getNumberOfSheets();
		for (int sheetIndex = 0; sheetIndex < numberOfSheets; sheetIndex++) {
			Sheet currentSheet = workbook.getSheetAt(sheetIndex);
			if (currentSheet.getSheetName().equalsIgnoreCase(
					ExcelUtil.EXCEL_COMPARE_SHEET_KEY)) {
				compareSheet = currentSheet;
				break;
			}
		}

		if (compareSheet != null) {
			Iterator<Row> iterator = compareSheet.iterator();
			while (iterator.hasNext()) {
				Row currentRow = iterator.next();
				if (isHeaderRow(workbook, currentRow, "Ignore")) { // skip
																	// header
																	// row
					continue;
				} else {

					// read 'ignore'
					boolean ignore = currentRow.getCell(
							EXCEL_COMPARE_IGNORE_COL).getBooleanCellValue();
					if (ignore) {
						continue; // skip to next row
					}

					// read 'scenarioName'
					String scenarioName = "";
					if (currentRow.getCell(EXCEL_COMPARE_SCENARIO_COL)
							.getStringCellValue() == null) {
						continue; // skip to next row
					} else {
						scenarioName = currentRow.getCell(
								EXCEL_COMPARE_SCENARIO_COL)
								.getStringCellValue();
					}

					// read 'className'
					String className = "";
					if (currentRow.getCell(EXCEL_COMPARE_CLASS_COL)
							.getStringCellValue() == null) {
						continue; // skip to next row
					} else {
						className = currentRow.getCell(EXCEL_COMPARE_CLASS_COL)
								.getStringCellValue();
					}

					// read 'methodName'
					String methodName = "";
					if (currentRow.getCell(EXCEL_COMPARE_METHOD_COL)
							.getStringCellValue() == null) {
						continue; // skip to next row
					} else {
						methodName = currentRow.getCell(
								EXCEL_COMPARE_METHOD_COL).getStringCellValue();
					}

					// read 'dateSource'
					String dateSource = "";
					if (currentRow.getCell(EXCEL_COMPARE_DATE_SOURCE_COL)
							.getStringCellValue() == null) {
						continue; // skip to next row
					} else {
						dateSource = currentRow.getCell(
								EXCEL_COMPARE_DATE_SOURCE_COL)
								.getStringCellValue();
					}

					// read 'dateCompare'
					String dateCompare = "";
					if (currentRow.getCell(EXCEL_COMPARE_DATE_COMPARE_COL)
							.getStringCellValue() == null) {
						continue; // skip to next row
					} else {
						dateCompare = currentRow.getCell(
								EXCEL_COMPARE_DATE_COMPARE_COL)
								.getStringCellValue();
					}

					CompareData CompareData = new CompareData(ignore,
							scenarioName, className, methodName, dateSource,
							dateCompare);
					compareList.add(CompareData);
				}
			}
		}

		return compareList;
	}
}
