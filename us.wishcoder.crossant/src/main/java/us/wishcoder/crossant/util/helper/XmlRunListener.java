package us.wishcoder.crossant.util.helper;

import org.junit.runner.Description;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import us.wishcoder.crossant.launcher.CrossantEnvironment;
import us.wishcoder.crossant.util.DateUtils;
import us.wishcoder.crossant.util.XmlUtil;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import java.io.*;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * This {@link RunListener} produces an XML report formatted like Ant's JUnit
 * XML report.
 * 
 * @author Ajay Singh
 * @taskpackage org.junit.junit4
 * @version 1.0
 * @since 2016-01-01
 */
public class XmlRunListener extends RunListener {
	protected static final Logger LOG = LoggerFactory
			.getLogger(XmlRunListener.class);

	private OutputStream outputStream;
	/**
	 * The XML document.
	 */
	private Document m_doc;
	/**
	 * The wrapper for the whole testsuite.
	 */
	private Element m_rootElement;
	/**
	 * Mapping between test Description:s -> Start timestamp (Long)
	 */
	private Map m_testStarts = new HashMap();
	/**
	 * Mapping between test Description:s -> Failure objects
	 */
	private Map m_failedTests = new HashMap();
	/**
	 * Mapping between test Description:s -> XML Element:s
	 */
	private Map m_testElements = new HashMap();

	private static String getTestCaseName(String s) {
		if (s == null) {
			return "unknown";
		}

		if (s.endsWith(")")) {
			int paren = s.lastIndexOf('(');
			return s.substring(0, paren);
		} else {
			return s;
		}
	}

	private static String getTestCaseClassName(String s) {
		if (s == null) {
			return "unknown";
		}

		// JUnit 4 wraps solo tests this way. We can extract
		// the original test name with a little hack.
		int paren = s.lastIndexOf('(');
		if (paren != -1 && s.endsWith(")")) {
			return s.substring(paren + 1, s.length() - 1);
		} else {
			return s;
		}
	}

	private static DocumentBuilder getDocumentBuilder() {
		try {
			return DocumentBuilderFactory.newInstance().newDocumentBuilder();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public XmlRunListener(String testSuiteStr) {
		if (testSuiteStr != null) {
			File target = new File(CrossantEnvironment.getResultPath()
					+ File.separator + testSuiteStr);
			target.mkdirs();
			
			
			target = new File(CrossantEnvironment.getResultPath()
					+ File.separator + testSuiteStr + File.separator + "JUnit_Result_" + DateUtils.format(new Date(),
							DateUtils.CUSTOM_DATETIME_PATTERN));
			try {
				this.outputStream = new BufferedOutputStream(
						new FileOutputStream(target));
				LOG.info("Writing test results to " + target.getAbsolutePath());
			} catch (FileNotFoundException e) {
				throw new RuntimeException(e);
			}
		}
	}

	/**
	 * XmlRunListener
	 * 
	 * @param outputStream
	 */
	public XmlRunListener(OutputStream outputStream) {
		setOutputStream(outputStream);
	}

	public void setOutputStream(OutputStream outputStream) {
		this.outputStream = outputStream;
	}

	/**
	 * get the local hostname
	 * 
	 * @return the name of the local host, or "localhost" if we cannot work it
	 *         out
	 */
	private String getHostname() {
		try {
			return InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			return "localhost";
		}
	}

	/**
	 * The whole test suite started.
	 * 
	 * @param descr
	 *            The test suite description.
	 */
	public void testRunStarted(Description descr) {
		m_doc = getDocumentBuilder().newDocument();
		m_rootElement = m_doc.createElement(XmlUtil.TESTSUITE);

		String n = descr.getDisplayName();
		m_rootElement
				.setAttribute(XmlUtil.ATTR_NAME, n == null ? "unknown" : n);

		// add the timestamp
		final String timestamp = DateUtils.format(new Date(),
				DateUtils.ISO8601_DATETIME_PATTERN);
		m_rootElement.setAttribute(XmlUtil.TIMESTAMP, timestamp);
		// and the hostname.
		m_rootElement.setAttribute(XmlUtil.HOSTNAME, XmlUtil.getHostname());

		// Output properties
		Element propsElement = m_doc.createElement(XmlUtil.PROPERTIES);
		m_rootElement.appendChild(propsElement);
		// Where do these come from?
		// Properties props = suite.getProperties();
		// if (props != null)
		// {
		// Enumeration e = props.propertyNames();
		// while (e.hasMoreElements())
		// {
		// String name = (String) e.nextElement();
		// Element propElement = doc.createElement(PROPERTY);
		// propElement.setAttribute(ATTR_NAME, name);
		// propElement.setAttribute(ATTR_VALUE, props.getProperty(name));
		// propsElement.appendChild(propElement);
		// }
		// }
	}

	/**
	 * Interface TestListener.
	 * <p/>
	 * <p>
	 * A new Test is started.
	 * 
	 * @param descr
	 *            The test description.
	 */
	public void testStarted(Description descr) {
		m_testStarts.put(descr, new Long(System.currentTimeMillis()));
	}

	private void formatError(String type, Failure f) {
		testFinished(f.getDescription());
		m_failedTests.put(f.getDescription(), f);

		Element nested = m_doc.createElement(type);
		Element currentTest = (Element) m_testElements.get(f.getDescription());

		currentTest.appendChild(nested);

		String message = f.getMessage();
		if (message != null && message.length() > 0) {
			nested.setAttribute(XmlUtil.ATTR_MESSAGE, message);
		}
		nested.setAttribute(XmlUtil.ATTR_TYPE, f.getDescription()
				.getDisplayName());

		String strace = XmlUtil.getFilteredTrace(f.getException());
		Text trace = m_doc.createTextNode(strace);
		nested.appendChild(trace);
	}

	/**
	 * Interface TestListener for JUnit &lt;= 3.4.
	 * <p/>
	 * <p>
	 * A Test failed.
	 * 
	 * @param f
	 *            The failure.
	 */
	public void testFailure(Failure f) {
		formatError(XmlUtil.FAILURE, f);
	}

	public void testAssumptionFailure(Failure f) {
		formatError(XmlUtil.FAILURE, f);
	}

	/**
	 * Interface TestListener.
	 * <p/>
	 * <p>
	 * A Test is finished.
	 * 
	 * @param descr
	 *            The test description.
	 */
	public void testFinished(Description descr) {
		// Fix for bug #5637 - if a junit.extensions.TestSetup is
		// used and throws an exception during setUp then startTest
		// would never have been called
		if (!m_testStarts.containsKey(descr)) {
			testStarted(descr);
		}

		Element currentTest = null;
		if (!m_failedTests.containsKey(descr)) {
			// Test test = (Test) descr.getAnnotation(Test.class);

			currentTest = m_doc.createElement(XmlUtil.TESTCASE);
			String n = getTestCaseName(descr.getDisplayName());
			currentTest.setAttribute(XmlUtil.ATTR_NAME, n == null ? "unknown"
					: n);
			// a TestSuite can contain Tests from multiple classes,
			// even tests with the same name - disambiguate them.
			currentTest.setAttribute(XmlUtil.ATTR_CLASSNAME,
					getTestCaseClassName(descr.getDisplayName()));
			m_rootElement.appendChild(currentTest);
			m_testElements.put(descr, currentTest);
		} else {
			currentTest = (Element) m_testElements.get(descr);
		}

		Long l = (Long) m_testStarts.get(descr);
		currentTest
				.setAttribute(
						XmlUtil.ATTR_TIME,
						""
								+ ((System.currentTimeMillis() - l.longValue()) / XmlUtil.ONE_SECOND));
	}

	/**
	 * The whole test suite ended.
	 * 
	 * @param result
	 *            The test suite result.
	 * @throws BuildException
	 *             on error.
	 */
	public void testRunFinished(Result result) {
		try {
			try {
				m_rootElement.setAttribute(XmlUtil.ATTR_TESTS,
						"" + result.getRunCount());
				m_rootElement.setAttribute(XmlUtil.ATTR_FAILURES,
						"" + result.getFailureCount());
				// JUnit4 does not seem to discern between failures and errors.
				m_rootElement.setAttribute(XmlUtil.ATTR_ERRORS, "" + 0);
				m_rootElement.setAttribute(XmlUtil.ATTR_TIME,
						"" + (result.getRunTime() / XmlUtil.ONE_SECOND));
				Writer wri = new BufferedWriter(new OutputStreamWriter(
						outputStream, "UTF8"));
				wri.write("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n");
				(new DOMElementWriter()).write(m_rootElement, wri, 0, "  ");
				wri.flush();
			} finally {
				outputStream.close();
			}
		} catch (IOException exc) {
			throw new RuntimeException("Unable to write log file", exc);
		}
	}

	protected void finalize() throws Throwable {
		if (outputStream != null) {
			outputStream.close();
		}
		super.finalize();
	}

	//
	// /**
	// * Where to write the log to.
	// */
	// private OutputStream out;
	//
	// /** {@inheritDoc}. */
	// public void setOutput(OutputStream out)
	// {
	// this.out = out;
	// }
	//
	// /** {@inheritDoc}. */
	// public void setSystemOutput(String out)
	// {
	// formatOutput(SYSTEM_OUT, out);
	// }
	//
	// /** {@inheritDoc}. */
	// public void setSystemError(String out)
	// {
	// formatOutput(SYSTEM_ERR, out);
	// }
	//
	// private void formatOutput(String type, String output)
	// {
	// Element nested = doc.createElement(type);
	// rootElement.appendChild(nested);
	// nested.appendChild(doc.createCDATASection(output));
	// }

}
