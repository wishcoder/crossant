package us.wishcoder.crossant.launcher;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import us.wishcoder.crossant.netty.Notification;
import us.wishcoder.crossant.netty.NotificationCallback;
import us.wishcoder.crossant.netty.ScenarioData;
import us.wishcoder.crossant.netty.ScenarioData.ScenarioSheet;
import us.wishcoder.crossant.netty.ScenarioData.Test;
import us.wishcoder.crossant.netty.server.NettyServer;
import us.wishcoder.crossant.util.DateUtils;
import us.wishcoder.crossant.util.ExcelUtil;
import us.wishcoder.crossant.util.ResultWriterUtil;
import us.wishcoder.crossant.util.SandboxUtil;

/**
 * <h1>Start Crossant server</h1>
 * <p>
 * <ul>
 * <li>Crossant clients communicate with server using Netty</li>
 * <li>Server session can start in following two modes:</li>
 * 		<br /><b>a. Stand alone mode:</b> Use <u>-Drunmode=standalone</u> VM argument. Please see {@link CrossantClientLauncher} for starting client sessions.
 * 		<br /><br /><b>b. Server mode:</b> Use <u>-Drunmode=server</u> VM argument. In this mode server and client sessions will be started automatically  and all scenario tests executed. 
 * </ul>
 * <br /><br /><b>NOTE:</b>This class <i>MUST</i> be executed in your project environment</li>
 *
 * 
 * @author Ajay Singh
 * @version 1.0
 * @since 2016-01-01
 * 
 *
 */
public class CrossantServerLauncher {
	// For Log4J Log File Name
	static {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
		System.setProperty("current.date.time", dateFormat.format(new Date()));
	}
	
	public static final String LOG_FOLDER = "logs";
	
	protected static final Logger LOG = LoggerFactory
			.getLogger(CrossantServerLauncher.class);

	private static int unitTestSessionsCount;
	private static CrossantEnvironment unitEnvironment;
	private static CountDownLatch waitLatch;
	private final NotificationCallback serverCallback = new ServerNotificationCallback();
	private static NettyServer nettyServer;
	private static Notification.Type PENDING_NOTIFICATION_TYPE;
	private static List<String> connectedClients;
	private Lock lock = new ReentrantLock(true);
	private static BlockingDeque<Notification> NotificationQueue = new LinkedBlockingDeque<Notification>();
	private volatile boolean isDispatching = true;

	/**
	 * Default private constructor
	 */
	private CrossantServerLauncher() throws Exception {
		try {
			LOG.info("\n\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>	CrossantServerLauncher - Start	<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n\n");

			connectedClients = new ArrayList<String>();
			unitEnvironment = initUnitEnvironment();

			unitTestSessionsCount = getSessionCount();
			if (unitTestSessionsCount == 0) {
				throw new Exception(
						"CrossantServerLauncher will exit due to no test found in test suite");
			} else {
				// initialize count down latch
				LOG.info("Test sessions count: " + unitTestSessionsCount);
				resetWaitLatch();
			}
		} catch (Exception e) {
			LOG.error(e.toString(), e);
			throw e;
		} finally {
			LOG.info("\n\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>	CrossantServerLauncher - Exit	<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n\n");
		}
	}

	/**
	 * Initialize system environment
	 * 
	 * @return CrossantEnvironment
	 * @throws Exception
	 */
	private CrossantEnvironment initUnitEnvironment() throws Exception {
		String baseClassPath = System.getProperty("user.dir");
		LOG.info("CrossantServerLauncher started in " + baseClassPath);

		// required testSuite
		String testSuite = CrossantEnvironment.getTestSuiteProperty();
		if ("".equals(testSuite)) {
			throw new Exception("Required environment property '"
					+ CrossantEnvironment.SYSTEM_PROPERTY_TEST_SUITE_KEY
					+ "' is missing");
		}else {
			LOG.info("testSuite: " + testSuite);
		}

		// required testSuitePath
		String testSuitePath = CrossantEnvironment.getTestsuitePathProperty()
				+ File.separator + testSuite;
		Path filePath = Paths.get(baseClassPath + File.separator
				+ testSuitePath);
		if (!Files.exists(filePath, LinkOption.NOFOLLOW_LINKS)) {
			throw new Exception("Unable to initialize test suite path '"
					+ testSuitePath + "'");
		}else {
			LOG.info("testSuitePath: " + testSuitePath);
		}

		// required testSuiteFilePath
		String testSuiteFilePath = CrossantEnvironment
				.getTestsuitePathProperty()
				+ File.separator
				+ testSuite
				+ File.separator
				+ CrossantEnvironment.getTestsuiteFileProperty();
		filePath = Paths
				.get(baseClassPath + File.separator + testSuiteFilePath);
		if (!Files.exists(filePath, LinkOption.NOFOLLOW_LINKS)) {
			throw new Exception("Required test suite file '"
					+ (baseClassPath + File.separator + testSuiteFilePath)
					+ "' is missing");
		}else {
			LOG.info("testSuiteFilePath: " + testSuiteFilePath);
		}

		// initialize CrossantEnvironment
		return new CrossantEnvironment(baseClassPath, testSuitePath, testSuite,
				testSuiteFilePath, System.getProperty("current.date.time"));
	}

	/**
	 * Get number of session counts from scenario excel file
	 * 
	 * @return int
	 * @throws Exception
	 */
	private int getSessionCount() throws Exception {
		int sessionCount = 0;
		Workbook sessionsBook = null;
		try {
			sessionsBook = ExcelUtil.getExcelWorkbook(unitEnvironment
					.getTestSuiteFilePath());

			sessionCount = ExcelUtil.getSessionCount(sessionsBook);
			LOG.info("Active session(s) count is " + sessionCount);
		} finally {
			if (sessionsBook != null) {
				sessionsBook.close();
			}
			sessionsBook = null;
		}
		return sessionCount;
	}

	/**
	 * Start netty server
	 * 
	 * @throws Exception
	 */
	public void startCrossantNettyServer() throws Exception {
		PENDING_NOTIFICATION_TYPE = Notification.Type.CLIENT_CONNECT;
		LOG.info("Started crossant netty server in '"
				+ CrossantEnvironment.getRunMode() + "' mode");
		nettyServer = new NettyServer(serverCallback, unitTestSessionsCount);
		nettyServer.start();

		// launch test suites only when running in server mode.
		if (CrossantEnvironment.isServerMode()) {
			startClientSandBox();
		} else {
			LOG.info("Crossant netty server is waiting for response from "
					+ unitTestSessionsCount + " client(s)...");
		}

		// start notification processing
		runNotificationLoop();
	}

	/**
	 * Run main notification loop
	 * 
	 * @throws Exception
	 */
	private void runNotificationLoop() throws Exception {
		while (isDispatching) {
			try {
				lock.lock();
				try {
					Object tmpObject = NotificationQueue.take(); // blocks
																	// until
																	// Event is
																	// available
					if (tmpObject != null && tmpObject instanceof Notification) {
						LOG.debug("Crossant server processing notification: " + (Notification) tmpObject);
						processNotification((Notification) tmpObject);
					}
				} finally {
					lock.unlock();
				}
			} catch (InterruptedException ex) {
				LOG.error("Notification loop stopped due to InterruptedException:  "
						+ ex, ex);
			} catch (Throwable ex) {
				LOG.error("Notification loop stopped due to Throwable:  " + ex, ex);
			}
		}
	}

	/**
	 * Process notifications
	 * 
	 * @param notification
	 * @throws Exception
	 */
	private void processNotification(Notification notification)
			throws Exception {
		LOG.info("Crossant netty server received '"
				+ notification.getType().toString()
				+ "' from crossant netty client " + notification.getSessionId());

		if (Notification.Type.CLIENT_CONNECTED == notification.getType()) {
			connectedClients.add(notification.getSessionId());
		}

		if (PENDING_NOTIFICATION_TYPE == Notification.Type.TEST_START) {
			processIntegratedTests(notification);
			return;
		}

		Notification response = new Notification(
				Notification.Type.SERVER_HEARTBEAT);
		response.setSessionId("");

		List<Notification.Type> notificationPair = Notification.TYPE_PAIR
				.get(PENDING_NOTIFICATION_TYPE);
		if (notificationPair == null || notificationPair.size() == 0) {
			return;
		}

		if (notificationPair.contains(notification.getType())) {
			waitLatch.countDown();
		}

		if (waitLatch.getCount() > 0) {
			LOG.info("crossant netty  server is waiting for "
					+ waitLatch.getCount()
					+ " crossant netty client(s) to respond to '"
					+ PENDING_NOTIFICATION_TYPE.toString() + "'");
			return;
		}

		switch (notification.getType()) {
		case CLIENT_CONNECTED:
			try {
				PENDING_NOTIFICATION_TYPE = Notification.Type.TEST_START;
				startIntegratedTests();
			} catch (Exception e) {
				this.isDispatching = false;
				nettyServer.shutdownServer();
			}
			return;
		case CLIENT_DISCONNECTED:
			if (waitLatch.getCount() == 0) {
				this.isDispatching = false;

				// shut down server
				nettyServer.shutdownServer();
				return;
			}
			break;
		default:
			// no need to notify client
			return;
		}

		PENDING_NOTIFICATION_TYPE = response.getType();
		resetWaitLatch();
		for (String client : connectedClients) {
			response.setSessionId(client);
			nettyServer.notifyClient(response);

			try {
				TimeUnit.MICROSECONDS.sleep(100);
			} catch (InterruptedException e) {
				LOG.error("Error in processNotification: " + e.getMessage(), e);
			}
		}
	}

	/**
	 * Load test suite in client sandbox
	 * 
	 * @throws Exception
	 */
	private void startClientSandBox() throws Exception {
		Workbook testSuite = null;
		try {
			String javaTaskLogDirPath = System.getProperty("user.dir") + File.separator + CrossantServerLauncher.LOG_FOLDER;
			File javaTaskLogDir = new File(javaTaskLogDirPath);
			// delete existing log dir if exists
			try {
				LOG.info("Cleaning log target path: " + javaTaskLogDir);
				FileUtils.cleanDirectory(javaTaskLogDir);
			}catch(Exception e) {
				// ignore
			}
			
			testSuite = ExcelUtil.getExcelWorkbook(unitEnvironment
					.getTestSuiteFilePath());

			int numberOfSheets = testSuite.getNumberOfSheets();

			for (int sheetIndex = 0; sheetIndex < numberOfSheets; sheetIndex++) {
				Sheet currentSheet = testSuite.getSheetAt(sheetIndex);
				if (currentSheet
						.getSheetName()
						.toLowerCase()
						.startsWith(
								ExcelUtil.EXCEL_SCENARIO_SHEET_KEY
										.toLowerCase())) {
					break;
				}

				// launch individual client session in forked JVM
				LOG.info("Now processing user session " + currentSheet.getSheetName() + " ...");
				
				SandboxUtil.launchSandbox(currentSheet.getSheetName(),
						unitEnvironment);

				LOG.info("User session " + currentSheet.getSheetName() + " processed");
				
				try {
					TimeUnit.SECONDS.sleep(10); // wait 10 seconds
				} catch (InterruptedException e) {
					LOG.error("Error in startClientSandBox: " + e.getMessage(), e);
				}
			}

			// exit launcher
			SandboxUtil.shutdownAll();
		} finally {
			if (testSuite != null) {
				testSuite.close();
			}
			testSuite = null;
		}

	}

	/**
	 * Start integrated tests when server is running in 'server' mode
	 * 
	 * @throws Exception
	 */
	private void startIntegratedTests() throws Exception {
		Workbook testSuiteBook = null;
		String integratedTestUID = DateUtils.format(new Date(),
				DateUtils.CUSTOM_DATETIME_PATTERN);
		try {
			testSuiteBook = ExcelUtil.getExcelWorkbook(unitEnvironment
					.getTestSuiteFilePath());

			int scenarioCount = ExcelUtil.getScenarioCount(testSuiteBook);
			if (scenarioCount == 0) {
				String error = "No test scenario found in "
						+ unitEnvironment.getTestSuite();
				LOG.error(error);
				throw new Exception(error);
			}

			ScenarioData scenarioData = ExcelUtil.getScenarioData(
					testSuiteBook, integratedTestUID);
			if (scenarioData == null) {
				String error = "Test scenario(s) makred 'ignore' or doesn't have tests in "
						+ unitEnvironment.getTestSuite();
				LOG.error(error);
				throw new Exception(error);
			}

			Notification clientNotification = new Notification(
					PENDING_NOTIFICATION_TYPE); // should be TEST_START

			LOG.debug(scenarioData.toString());
			resetWaitLatch(scenarioData.getScenarioCount());

			Test firstTest = scenarioData.getFirstTest();
			firstTest.setStatus(ScenarioData.Status.READY);
			firstTest.getParentScenarioSheet().setStatus(
					ScenarioData.Status.READY);

			clientNotification.setScenarioData(scenarioData.clone());
			clientNotification.setSessionId(firstTest.getUser());

			// notify only first client. other clients will be notified by
			// current test upon completion
			nettyServer.notifyClient(clientNotification);
		} finally {
			if (testSuiteBook != null) {
				testSuiteBook.close();
			}
			testSuiteBook = null;
		}
	}

	/**
	 * Process client response for integrated tests and reset wait latch
	 * 
	 * @param notification
	 */
	private void processIntegratedTests(Notification notification) {
		ScenarioData scenarioData = notification.getScenarioData();
		if (waitLatch.getCount() > 0) {
			// update status in ScenarioData
			scenarioData.updateScenarioSheetStatus();

			// update waitLatch
			for (ScenarioSheet scenarioSheet : scenarioData.getScenarioSheets()) {
				if(!scenarioSheet.isProcessed() && scenarioSheet.isFailFast() && notification.getType() == Notification.Type.TEST_FAIL){
					// 'Fail Fast' 
					waitLatch = new CountDownLatch(0);
					LOG.error("Stopping tests [isFailFast= " + scenarioSheet.isFailFast() + ", notificationType= " + notification.getType() + "");
					break;
				}else{
					if (scenarioSheet.getStatus() == ScenarioData.Status.DONE
							&& !scenarioSheet.isProcessed()) {
						waitLatch.countDown();
						scenarioSheet.setProcessed(true);
					}
				}
			}
		}

		if (waitLatch.getCount() == 0) { // all scenarios/tests are executed
			LOG.info("All scenarios/tests are executed");
			resetWaitLatch();

			// write test result
			try {
				File resultTargetPath = ResultWriterUtil.writeTestResult(
						unitEnvironment.getTestSuite(), notification, System.getProperty("current.date.time"));
				if(null != resultTargetPath) {
					try {
						ResultWriterUtil.generateHtmlReport(resultTargetPath, unitEnvironment.getTestSuite());
					}catch(Exception e) {
						LOG.error("Exception in processing generateHtmlReport: " + e.getMessage(), e);
					}
				}			
			} catch (Exception e1) {
				LOG.error("Exception is writeTestResult: "
						+ e1.getMessage(), e1);
			}

			PENDING_NOTIFICATION_TYPE = Notification.Type.CLIENT_DISCONNECT;
			Notification response = new Notification(
					Notification.Type.CLIENT_DISCONNECT);
			for (String client : connectedClients) {
				response.setSessionId(client);
				nettyServer.notifyClient(response);

				try {
					TimeUnit.MICROSECONDS.sleep(100);
				} catch (InterruptedException e) {
					LOG.error("Error in processIntegratedTests: "
							+ e.getMessage(), e);
				}
			}
		} else { // some tests are still pending. notify next client to execute
					// test in time line sequence for a specific scenario
			Test readyTest = scenarioData.getReadyTest();
			if (readyTest != null) {
				notification.setSessionId(readyTest.getUser());
				notification.setType(PENDING_NOTIFICATION_TYPE); // should be
																	// TEST_START
				nettyServer.notifyClient(notification);
			}
		}
	}

	/**
	 * reset wait latch
	 */
	private static void resetWaitLatch() {
		waitLatch = new CountDownLatch(unitTestSessionsCount);
	}

	/**
	 * reset wait latch with waitCount
	 * 
	 * @param waitCount
	 */
	private static void resetWaitLatch(int waitCount) {
		waitLatch = new CountDownLatch(waitCount);
	}

	/**
	 * ServerNotificationCallback
	 * 
	 * @author ajsingh
	 * 
	 */
	private static class ServerNotificationCallback implements
			NotificationCallback {
		protected static final Logger LOG = LoggerFactory
				.getLogger(ServerNotificationCallback.class);

		/**
		 * 
		 */
		public ServerNotificationCallback() {
			// TODO Auto-generated constructor stub
		}

		public void onError(Object msg, Throwable throwable) {
			LOG.error("onError", throwable);
		}

		public synchronized void onData(Object msg) throws Exception {
			NotificationQueue.offerLast((Notification) msg);
		}

		public void onConnect() {
			LOG.info("onConnect");
		}

		public void onDisconnect() {
			LOG.info("onDisconnect");
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		CrossantServerLauncher launcher = new CrossantServerLauncher();
		launcher.startCrossantNettyServer();

	}

}
