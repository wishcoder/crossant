package us.wishcoder.crossant.launcher;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import us.wishcoder.crossant.netty.Notification;
import us.wishcoder.crossant.netty.NotificationCallback;
import us.wishcoder.crossant.netty.client.NettyClient;
import us.wishcoder.crossant.util.TestRunnerUtil;

/**
 * <h1>Start new client session for every user tab in Scenario excel file</h1>
 * <p>
 * <ul>
 * <li>Client session starts netty client and connect to netty server</li> 
 * <li>Client communicate with server using Netty</li>
 * <li>Client session can start in following two modes:</li>
 * 		<br /><b>a. Stand alone mode:</b> Use <u>-Drunmode=standalone</u> VM argument. Crossant server <i>MUST</i> be running in stand alone mode before starting client in this mode.
 * 			<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Running Crossant Server:</b>us.wishcoder.crossant.launcher.CrossantServerLauncher -Drunmode=standalone -Dtestsuite=suite1 
 * 			<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Running Crossant Client:</b>us.wishcoder.crossant.launcher.CrossantClientLauncher -Drunmode=standalone
 * 			<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Client Program Arguments:</b>[Eclipse workspace location]\\us.wishcoder.marketplace.client\\target\\classes suites\\suite1 suite1 suites\\suite1\\TestSuite.xlsx user1	
 * 		<br /><br /><b>b. Server mode:</b> Use <u>-Drunmode=server</u> VM argument. In this mode server and client sessions will be started automatically  and all scenario tests executed. 
 * </ul>
 * <br /><br /><b>NOTE:</b>This class <i>MUST</i> be executed in your project environment</li>
 * 
 * @author Ajay Singh
 * @version 1.0
 * @since 2016-01-01
 * 
 *
 */

public class CrossantClientLauncher {

	protected static final Logger LOG = LoggerFactory

	.getLogger(CrossantClientLauncher.class);

	private final NotificationCallback clientCallback = new ClientNotificationCallback();

	private final String baseClassPath;

	private NettyClient crossantNettyClient;

	private final String testSuitePath;

	private final String testSuite;

	private final String testSuiteFilePath;

	private final String crossantClientId;

	/**
	 * 
	 * @param baseClassPath
	 * 
	 * @param testSuitePath
	 * 
	 * @param testSuite
	 * 
	 * @param testSuiteFilePath
	 * 
	 * @param crossantClientId
	 */

	private CrossantClientLauncher(String baseClassPath,

	String testSuitPath, String testSuit, String testSuiteFilePath,

	String sessionId) throws Exception {

		this.baseClassPath = baseClassPath;

		this.testSuitePath = testSuitPath;

		this.testSuite = testSuit;

		this.testSuiteFilePath = testSuiteFilePath;

		this.crossantClientId = sessionId;

		LOG.info("Starting crossant client " + this.crossantClientId + " in '"
				+ CrossantEnvironment.getRunMode() + "' mode");

		LOG.info(this.toString());

		if (CrossantEnvironment.isClientMode()) {

			startClientInClientMode();

		} else {

			startClientInIntegratedMode();

		}

	}

	/**
	 * Absolute path to your project compiled classes 
	 * 
	 * @return the baseClassPath
	 */

	public String getBaseClassPath() {

		return baseClassPath;

	}

	/**
	 * Relative path from src/main/resources to the folder containing 'TestSuite.xlsx' scenario configuration file
	 * 
	 * @return the testSuitePath
	 */

	public String getTestSuitePath() {

		return testSuitePath;

	}

	/**
	 * Name of the folder containing 'TestSuite.xlsx' scenario configuration file
	 * 
	 * @return the testSuite
	 */

	public String getTestSuite() {

		return testSuite;

	}

	/**
	 * Relative path of 'TestSuite.xlsx' scenario configuration file from src/main/resources
	 * 
	 * @return the testSuiteFilePath
	 */

	public String getTestSuiteFilePath() {

		return testSuiteFilePath;

	}

	/**
	 * User defined client session id
	 * 
	 * @return the crossantClientId
	 */

	public String getCrossantClientId() {

		return crossantClientId;

	}

	/**
	 * Start crossant client in integrated mode. Server has to be running for
	 * this mode.
	 * 
	 * @throws Exception
	 */

	private void startClientInIntegratedMode() throws Exception {

		crossantNettyClient = new NettyClient(clientCallback,
				getCrossantClientId());

		crossantNettyClient.start();

		// send notification to crossant server

		Notification request = new Notification(

		Notification.Type.CLIENT_CONNECTED);

		request.setSessionId(this.crossantClientId);

		notifyServer(request);

	}

	/**
	 * Start crossant in client mode. In this mode there is no need to start
	 * server
	 * 
	 * @throws Exception
	 */

	private void startClientInClientMode() throws Exception {
		try {
			executeClientOnlyTestSuite();
		} finally {
			closeSessions();
		}
	}

	/**
	 * Send notification to server
	 * 
	 * @param notification
	 */

	private void notifyServer(Notification notification) throws Exception {

		if (crossantNettyClient != null) {

			crossantNettyClient.notifyServer(notification);

			if (notification.getType() == Notification.Type.CLIENT_DISCONNECTED) {

				closeSessions();

			}

		} else {
			LOG.error("crossantNettyClient is null");
		}

	}

	/**
	 * Execute stand alone test suite
	 * 
	 * @throws Exception
	 */

	private void executeClientOnlyTestSuite() throws Exception {

		try {

			TestRunnerUtil.executeStandaloneTests(getCrossantClientId(),
					(getBaseClassPath()

					+ File.separator + getTestSuitePath()), getTestSuite(),

					getTestSuiteFilePath());

		} catch (Exception e) {

			throw e;

		}

	}

	/**
	 * Execute integrated test
	 * 
	 * @throws Exception
	 */

	private void executeIntegratedTest(Notification notification)
			throws Exception {

		Notification clientNotification = new Notification(
				Notification.Type.TEST_PASS);

		try {

			clientNotification = TestRunnerUtil.executeIntegratedTests(
					getCrossantClientId(), getTestSuite(), notification);

		} catch (Exception e) {

			clientNotification = new Notification(Notification.Type.TEST_FAIL);
			LOG.error(notification.getScenarioData().toString() + " failed due to: " + e.getMessage(), e );
			throw e;

		} finally {

			clientNotification.setSessionId(this.crossantClientId);

			clientNotification.setScenarioData(notification.getScenarioData());

			notifyServer(clientNotification);

		}

	}

	/**
	 * Close client session
	 * 
	 * @throws Exception
	 */

	private void closeSessions() throws Exception {

		LOG.info("Crossant client session closed");

	}

	@Override
	public String toString() {

		return "CrossantClientLauncher{baseClassPath=" + baseClassPath

		+ ", testSuitePath=" + testSuitePath + ", testSuite=" + testSuite

		+ ", testSuiteFilePath=" + testSuiteFilePath + ", crossantClientId="

		+ crossantClientId + ", isIntegratedMode="
				+ CrossantEnvironment.isServerMode() + "}";

	}

	/**
	 * ClientNotificationCallback
	 */

	private class ClientNotificationCallback implements

	NotificationCallback {

		protected final Logger LOG = LoggerFactory

		.getLogger(ClientNotificationCallback.class);

		public ClientNotificationCallback() {
		}

		public void onError(Object msg, Throwable throwable) {
			LOG.error("onError: " + msg, throwable);
		}

		public synchronized void onData(Object msg) throws Exception {

			Notification response = (Notification) msg;

			if (!response.getSessionId()
					.equalsIgnoreCase(getCrossantClientId())) {
				LOG.error("Session id " + response.getSessionId() + " does not match with Client id " + getCrossantClientId());
				return;

			}

			LOG.info("crossant client " + response.getSessionId()

			+ " received '" + response.getType().toString()

			+ "' from crossant server");

			Notification request = new Notification(

			Notification.Type.CLIENT_HEARTBEAT);

			request.setSessionId("");

			switch (response.getType()) {

			case CLIENT_DISCONNECT:

				request = new Notification(

				Notification.Type.CLIENT_DISCONNECTED);

				break;

			case TEST_START:

				executeIntegratedTest(response);

				return;

			case TEST_STOP:

				request = new Notification(Notification.Type.TEST_STOPPED);

				break;

			default:
				return;
			}

			request.setSessionId(response.getSessionId());

			notifyServer(request);

		}

		public void onConnect() {
			LOG.info("onConnect");

		}

		public void onDisconnect() {
			LOG.info("onDisconnect");
		}
	}

	/**
	 * main
	 * 
	 * @param args
	 * 
	 * @throws Exception
	 */

	public static void main(String[] args) throws Exception {

		LOG.info("CrossantClientLauncher.main.args.length: " + args.length);

		if (args.length < 5) {
			throw new Exception(
					"Missing arguments: baseClassPath, testSuitePath, testSuite, testSuiteFilePath, crossantClientId");
		}

		Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(LOG));

		new CrossantClientLauncher(args[0], args[1], args[2], args[3],

		args[4]);
	}
}

/**
 * 
 * UncaughtExceptionHandler
 * 
 */

class ExceptionHandler implements Thread.UncaughtExceptionHandler {

	private static Logger LOG;

	public ExceptionHandler(Logger _LOG) {

		LOG = _LOG;

	}

/**
 * uncaughtException
 * 
 * @param t
 * @param e
 */
	public void uncaughtException(Thread t, Throwable e) {

		LOG.error("UncaughtExceptionHandler: "

		+ e, e);
	}
}
