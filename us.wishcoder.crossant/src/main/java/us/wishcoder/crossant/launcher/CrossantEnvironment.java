package us.wishcoder.crossant.launcher;



import java.io.File;

/**
 * <h1>Environment variables required to run Crossant</h1>
 * <p>
 * <ul>
 * <li><b>[optional] testsuitefile:</b>-Dtestsuitefile=abc.xlsx, <b>default:</b>TestSuite.xlsx</li>
 * <li><b>[required] testsuite:</b>-Dtestsuite=xyz</li> 
 * <li><b>[optional] testsuitespath:</b>-Dtestsuitespath=abc\xyz, <b>default:</b>System.getProperty("user.dir") + File.separator	+ "target" + File.separator + "classes" + File.separator + testSuite</li> 
 * <li><b>[optional] testSuiteFilePath:</b>-DtestSuiteFilePath=abc\xyz\abc.xlsx, <b>default:</b>System.getProperty("user.dir") + File.separator	+ "target" + File.separator + "classes" + File.separator + testSuite + File.separator + testsuitefile</li>  
 * </ul>
 * 
 * @author Ajay Singh
 * @version 1.0
 * @since 2016-01-01
 * 
 *
 */

public class CrossantEnvironment {
	public static final String JUNIT_REPORT_IGNORE_TEST = System.getProperty("ignoreMethod", "tearDownAfterTest");
	public static final String ANT_BUILD_PATH = "ant";
	
	public static final String RESULT_FOLDER_PATH = "results" + File.separator
			+ "suites";

	public static final String COMPARE_RESULT_FOLDER_NAME = "compare";

	public static final String SYSTEM_PROPERTY_RUN_MODE_KEY = "runmode";

	// see isServerMode()

	public static final String RUN_MODE_TYPE_SERVER = "server";

	// see isClientMode()

	public static final String RUN_MODE_TYPE_CLIENT = "client";

	/************************* RUN_MODE_TYPE_STANDALONE *************************/

	// Run Server and Client(s) in 'standalone' mode to run/debug individual

	// server/client(s) instances.

	// Example:

	// src\main\resources\suites\suite1\TestSuite.xlsx has following

	// configuration:

	// Sessions: user1, user2

	// Scenario-1: 
	// 1. user1 enter Sell
	// 2. user2 enter Buy


	// Start sequence:

	// 1. Start server in 'standalone' mode

	// 2. Start client 'user1' in 'standalone' mode

	// 3. Start client 'user2' in 'standalone' mode

	// 4. This should start executing test sequence mentioned in 'Scenario-1'

	// 5. All client(s) and server instances should shut down after completion

	// of test sequence

	// Find sample Eclipse Launch configurations for above scenario in:

	// \src\main\resources\eclipse\launch_configuration\*

	/********************************************************************************/

	public static final String RUN_MODE_TYPE_STANDALONE = "standalone";

	/**
	 * 
	 * Tests will run in server mode. Client tests will start in separate
	 * 
	 * sandbox and will communicate response back to server. Don't start
	 * 
	 * separate client instances
	 * 
	 * 
	 * 
	 * @return boolean
	 */

	public static boolean isServerMode() {

		return System.getProperty(SYSTEM_PROPERTY_RUN_MODE_KEY,

		RUN_MODE_TYPE_STANDALONE)

		.equalsIgnoreCase(RUN_MODE_TYPE_SERVER);

	}

	/**
	 * 
	 * Tests will start in standalone mode and will execute tests listed in user
	 * 
	 * session work sheet. Don't start server instance
	 * 
	 * 
	 * 
	 * @return
	 */

	public static boolean isClientMode() {

		return System.getProperty(SYSTEM_PROPERTY_RUN_MODE_KEY,

		RUN_MODE_TYPE_STANDALONE)

		.equalsIgnoreCase(RUN_MODE_TYPE_CLIENT);

	}

	/**
	 * 
	 * Return run mode. Default is 'standalone' mode
	 * 
	 * 
	 * 
	 * @return mode
	 */

	public static String getRunMode() {

		return System.getProperty(SYSTEM_PROPERTY_RUN_MODE_KEY,

		RUN_MODE_TYPE_STANDALONE);

	}

	// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

	public static final String SYSTEM_PROPERTY_TEST_SUITE_KEY = "testsuite";

	/**
	 * 
	 * This is a REQUIRED system property to run integrated or seam less mode
	 * 
	 * 
	 * 
	 * @return String
	 */

	public static String getTestSuiteProperty() {

		return System.getProperty(SYSTEM_PROPERTY_TEST_SUITE_KEY, "");

	}

	// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

	public static final String SYSTEM_PROPERTY_CLASSPATH_KEY = "classpathfile";

	public static final String CLASSPATH_FILE = "classpath.xlsx";

	/**
	 * 
	 * This is a OPTIONAL system property. Default value is classpath.xlsx This
	 * 
	 * file contains all class path entries required to run JVM
	 * 
	 * 
	 * 
	 * @return String
	 */

	public static String getClasspathFileProperty() {

		return System

		.getProperty(SYSTEM_PROPERTY_CLASSPATH_KEY, CLASSPATH_FILE);

	}

	// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

	public static final String SYSTEM_PROPERTY_TEST_SUITE_FILE_KEY = "testsuitefile";

	public static final String TEST_SUITE_FILE = "TestSuite.xlsx";

	/**
	 * 
	 * This is a OPTIONAL system property. Default value is TestSuite.xlsx This
	 * 
	 * file contains all sessions and tests details
	 * 
	 * 
	 * 
	 * @return String
	 */

	public static String getTestsuiteFileProperty() {

		return System.getProperty(SYSTEM_PROPERTY_TEST_SUITE_FILE_KEY,

		TEST_SUITE_FILE);

	}

	// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

	public static final String SYSTEM_PROPERTY_TEST_SUITS_PATH_KEY = "testsuitespath";

	public static final String TEST_SUITES_PATH = "suites";

	/**
	 * 
	 * This is a OPTIONAL system property. Default value is 'suites' This is he
	 * 
	 * location of all test suites
	 * 
	 * 
	 * 
	 * @return String
	 */

	public static String getTestsuitePathProperty() {

		return System.getProperty(SYSTEM_PROPERTY_TEST_SUITS_PATH_KEY,

		TEST_SUITES_PATH);

	}

	/**
	 * 
	 * Result path where results will be stored
	 * 
	 * results/suites/suite1 etc.
	 * 
	 * @return path
	 */

	public static final String getResultPath() {

		return new File("").getAbsolutePath() + File.separator
				+ RESULT_FOLDER_PATH;

	}

	/**
	 * 
	 * Result path where compare results will be stored
	 * 
	 * results/suites/suite1/compare etc.
	 * 
	 * @return path
	 */

	public static final String getCompareResultPath(String suiteStr) {

		return new File("").getAbsolutePath() + File.separator
				+ RESULT_FOLDER_PATH + File.separator + suiteStr
				+ File.separator + COMPARE_RESULT_FOLDER_NAME;

	}

	/**
	 * 
	 * @return File
	 */
	public static final File getAntPath() {
		return new File(System.getProperty("user.dir") + File.separator + ANT_BUILD_PATH);
	}
	
	// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> CrossantEnvironment Class

	private final String baseClassPath;

	private final String testSuitePath;

	private final String testSuite;

	private final String testSuiteFilePath;
	
	private final String logDateTime;

	/**
	 * 
	 * 
	 * 
	 * @param baseClassPath
	 * 
	 * @param testSuitePath
	 * 
	 * @param testSuite
	 * 
	 * @param testSuiteFilePath
	 * 
	 * @param logDateTime
	 * 
	 * 
	 */

	public CrossantEnvironment(String baseClassPath, String testSuitePath,

	String testSuite, String testSuiteFilePath, String logDateTime) {

		this.baseClassPath = baseClassPath;

		this.testSuitePath = testSuitePath;

		this.testSuite = testSuite;

		this.testSuiteFilePath = testSuiteFilePath;
		
		this.logDateTime = logDateTime;
	}

	/**
	 * 
	 * @return the baseClassPath
	 */

	public String getBaseClassPath() {

		return baseClassPath;

	}

	/**
	 * 
	 * @return the testSuitePath
	 */

	public String getTestSuitePath() {

		return testSuitePath;

	}

	/**
	 * 
	 * @return the testSuite
	 */

	public String getTestSuite() {

		return testSuite;

	}

	/**
	 * 
	 * @return the testSuiteFilePath
	 */

	public String getTestSuiteFilePath() {

		return testSuiteFilePath;

	}
	
	/**
	 * 
	 * @return String
	 */
	public String getLogDateTime() {
		return logDateTime;
	}

	@Override
	public String toString() {
		return "CrossantEnvironment [baseClassPath=" + baseClassPath + ", testSuitePath=" + testSuitePath
				+ ", testSuite=" + testSuite + ", testSuiteFilePath=" + testSuiteFilePath + ", logDateTime="
				+ logDateTime + "]";
	}
	
}