package us.wishcoder.crossant.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
* <h1>Custom annotation for JUnit methods</h1>
* <p>Method level annotation that can be used in 
* JUnit test class to export application state in XML file
* </p>XML file will be saved in [approot]/results/suites directory
* 
*
* @author  Ajay Singh
* @version 1.0
* @since   2016-01-01 
* 
* <pre>
* <h3>Usage:</h3>
* //method in JUnit test class
* {@literal @}CrossantStateWriter
* public Map{@literal <}String, Object{@literal >} exportAppStates(){
* 	Map<String, Object> appStates = new HashMap<String, Object>();
* 	try{
* 		appStates.put("someKey", someObject);
* 		appStates.put("otherKey", otherObject);
* 	}catch(Exception e){
* 		// log error
* 		appStates.put("errorKey", e.getMessage());
* 	}	
* 	return appStates;
* }
* </pre>
*/


@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD) //can use in method only
public @interface CrossantStateWriter {

}
