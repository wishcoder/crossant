/**
 * 
 */
package us.wishcoder.crossant.netty;

/**
 * <h1>Callback interface</h1>
 *
 * @author Ajay Singh
 * @version 1.0
 * @since 2016-01-01
 * 
 *
 */
public interface NotificationCallback {
	void onError(Object msg, Throwable throwable);

	void onData(Object msg) throws Exception;

	void onConnect();

	void onDisconnect();
}
