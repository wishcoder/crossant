package us.wishcoder.crossant.netty;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.SerializationUtils;

/**
 * <h1>Object to communicate between netty server and client</h1>
 * 
 * @author Ajay Singh
 * @version 1.0
 * @since 2016-01-01
 * 
 *
 */
public class Notification implements Serializable {
	private static final long serialVersionUID = 6641122981875039614L;

	public static enum Type {
		CLIENT_CONNECT("CLIENT_CONNECT"), CLIENT_CONNECTED("CLIENT_CONNECTED"), CLIENT_DISCONNECT(
				"CLIENT_DISCONNECT"), CLIENT_DISCONNECTED("CLIENT_DISCONNECTED"), TEST_START(
				"TEST_START"), TEST_STOP("TEST_STOP"), TEST_STOPPED(
				"TEST_STOPPED"), TEST_PASS("TEST_PASS"), TEST_FAIL(
				"TEST_FAIL"), SERVER_HEARTBEAT("SERVER_HEARTBEAT"), CLIENT_HEARTBEAT(
				"CLIENT_HEARTBEAT");

		private String value;

		private Type(String value) {
			this.value = value;
		}

		@Override
		public String toString() {
			return this.value;
		}
	}

	public static final Map<Type, List<Type>> TYPE_PAIR = new HashMap<Notification.Type, List<Type>>();
	static {
		TYPE_PAIR.put(Type.CLIENT_CONNECT,
				Arrays.asList(new Type[] { Type.CLIENT_CONNECTED }));
		TYPE_PAIR.put(Type.CLIENT_DISCONNECT,
				Arrays.asList(new Type[] { Type.CLIENT_DISCONNECTED }));
		TYPE_PAIR
				.put(Type.TEST_START,
						Arrays.asList(new Type[] { Type.TEST_PASS,
								Type.TEST_FAIL }));
		TYPE_PAIR.put(Type.TEST_STOP,
				Arrays.asList(new Type[] { Type.TEST_STOPPED }));
	}

	private String SessionId;
	private Type type;
	private ScenarioData ScenarioData;
	
	/**
	 * NettySignals
	 */
	public Notification(Type type) {
		this.type = type;
	}

	/**
	 * @return the SessionId
	 */
	public String getSessionId() {
		return SessionId;
	}

	/**
	 * @param SessionId
	 *            the SessionId to set
	 */
	public void setSessionId(String SessionId) {
		this.SessionId = SessionId;
	}

	/**
	 * @return the type
	 */
	public Type getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(Type type) {
		this.type = type;
	}

	/**
	 * @return the ScenarioData
	 */
	public ScenarioData getScenarioData() {
		return ScenarioData;
	}

	/**
	 * @param ScenarioData the ScenerioData to set
	 */
	public void setScenarioData(ScenarioData ScenarioData) {
		this.ScenarioData = ScenarioData;
	}

	/**
	 * Used in netty codec
	 * 
	 * @return byte[]
	 */
	public byte[] toByteArray() {
		return SerializationUtils.serialize(this);
	}

	@Override
	public String toString() {
		return "Notification [SessionId=" + SessionId + ", type=" + type + ", ScenarioData=" + ScenarioData + "]";
	}
}
