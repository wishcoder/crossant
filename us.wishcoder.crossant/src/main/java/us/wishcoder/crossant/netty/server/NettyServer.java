package us.wishcoder.crossant.netty.server;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import us.wishcoder.crossant.netty.Notification;
import us.wishcoder.crossant.netty.NotificationCallback;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;



/**
 * <h1>This class is netty server</h1>
* <p>
* <h3>VM Arguments:</h3>
 * <ul>
 * <li><b>[optional] host:</b>-Dhost=localhost, <b>default:</b>127.0.0.1</li>
 * <li><b>[optional] port:</b>-Dport=1234, <b>default:</b>8463</li>
 * </ul>
 * 
 * @author Ajay Singh
 * @version 1.0
 * @since 2016-01-01
 * 
 *
 */
public class NettyServer {
	protected final Logger LOG = LoggerFactory
			.getLogger(NettyServer.class);
	
	static final int PORT = Integer
			.parseInt(System.getProperty("port", "8463"));

	private EventLoopGroup serverBossGroup;
	private EventLoopGroup serverWorkerGroup;	
	private NotificationCallback callback;
	private NettyServerInboundHandler requestHandler;
	private int unitTestCount;

	/**
	 * Constructor
	 * 
	 * @param callback  NotificationCallback
	 * @param unitTestCount  int
	 */
	public NettyServer(NotificationCallback callback, int unitTestCount) {
		this.callback = callback;
		this.unitTestCount = unitTestCount;
	}

	/**
	 * Start server
	 * 
	 * @throws Exception
	 */
	public void start() throws Exception {
		serverBossGroup = new NioEventLoopGroup(1);
		serverWorkerGroup = new NioEventLoopGroup();

		try {
			NettyServerInitializer NettyServerInitializer = new NettyServerInitializer(callback, unitTestCount);
			this.requestHandler = NettyServerInitializer.getRequestHandler();
					
			ServerBootstrap bootstrap = new ServerBootstrap();
			bootstrap.group(serverBossGroup, serverWorkerGroup)
					.channel(NioServerSocketChannel.class)
					.handler(new LoggingHandler(LogLevel.DEBUG))
					.childHandler(NettyServerInitializer)
					.validate();

			// Start the server.
			ChannelFuture channelFuture = bootstrap.bind(PORT);
			channelFuture.sync();
			
			final CountDownLatch channelLatch = new CountDownLatch(1);
			channelFuture.addListener(new ChannelFutureListener() {
				public void operationComplete(ChannelFuture cf) throws Exception {
					if (cf.isSuccess()) {
						channelLatch.countDown();
					} else {
						shutdownServer();
						throw new Exception(
								"crossant server Interrupted while waiting for streaming "
										+ "connection to arrive.");
					}
				}
			});

			try {
				channelLatch.await();
			} catch (InterruptedException ex) {
				throw new Exception(
						"crossant server Interrupted while waiting for streaming "
								+ "connection to arrive.");
			}
			
		} finally {
			//serverBossGroup.shutdownGracefully();
			//serverWorkerGroup.shutdownGracefully();
		}
	}

	/**
	 * Send notification to client
	 * 
	 * @param notification
	 */
	public synchronized void notifyClient(final Notification notification) {
		if(notification.getType() != Notification.Type.CLIENT_DISCONNECTED){
			LOG.info("crossant server sending '" + notification.getType().toString() + "' to crossant client " + notification.getSessionId());
			this.requestHandler.sendResponse(notification);			
		}
	}
	
	/**
	 * Shut down server
	 */
	public void shutdownServer(){
		serverBossGroup.shutdownGracefully(1000, 5000, TimeUnit.MILLISECONDS);
		serverWorkerGroup.shutdownGracefully(1000, 5000, TimeUnit.MILLISECONDS);
		LOG.info("crossant server shutdown");
	}
}
