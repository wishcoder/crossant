package us.wishcoder.crossant.netty.server;

import us.wishcoder.crossant.netty.NotificationCallback;
import us.wishcoder.crossant.netty.client.NettyClient;
import us.wishcoder.crossant.netty.client.NettyClientInboundHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.serialization.ClassResolvers;
import io.netty.handler.codec.serialization.ObjectDecoder;
import io.netty.handler.codec.serialization.ObjectEncoder;


/**
 * <h1>Initialize {@link NettyServerInboundHandler} for {@link NettyServer}</h1>
 *
 * @author Ajay Singh
 * @version 1.0
 * @since 2016-01-01
 * 
 *
 */
public class NettyServerInitializer extends ChannelInitializer<SocketChannel> {

	private NotificationCallback callback;
	private NettyServerInboundHandler requestHandler;

	/**
	 * 
	 * @param callback
	 */
	public NettyServerInitializer(NotificationCallback callback, int UnitTestCount) {
		this.callback = callback;
		this.requestHandler = new NettyServerInboundHandler(this.callback, UnitTestCount);
	}

	/**
	 * Initialize ChannelPipeline for {@link NettyServer}
	 * 
	 * @param ch  SocketChannel
	 * 
	 */
	@Override
	protected void initChannel(SocketChannel ch) throws Exception {
		final ChannelPipeline pipeline = ch.pipeline();
		// pipeline.addLast("logger", new LoggingHandler(LogLevel.INFO));
		pipeline.addLast("encoder", new ObjectEncoder());
		pipeline.addLast("decoder",
				new ObjectDecoder(ClassResolvers.cacheDisabled(null)));
		pipeline.addLast("requestHandler", this.requestHandler);
	}

	/**
	 * 
	 * @return NettyServerInboundHandler
	 */
	public NettyServerInboundHandler getRequestHandler() {
		return this.requestHandler;
	}
}
