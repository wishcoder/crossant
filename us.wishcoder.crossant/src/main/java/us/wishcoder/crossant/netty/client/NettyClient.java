package us.wishcoder.crossant.netty.client;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;




import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import us.wishcoder.crossant.launcher.CrossantClientLauncher;
import us.wishcoder.crossant.netty.Notification;
import us.wishcoder.crossant.netty.Notification.Type;
import us.wishcoder.crossant.netty.NotificationCallback;
import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;


/**
 * <h1>This class is netty client connected to server </h1>
* <p>
* <h3>VM Arguments:</h3>
 * <ul>
 * <li><b>[optional] host:</b>-Dhost=localhost, <b>default:</b>127.0.0.1</li>
 * <li><b>[optional] port:</b>-Dport=1234, <b>default:</b>8463</li>
 * </ul>
 * 
 * @author Ajay Singh
 * @version 1.0
 * @since 2016-01-01
 * 
 *
 */
public class NettyClient {
	protected final Logger LOG = LoggerFactory.getLogger(NettyClient.class);

	static final String HOST = System.getProperty("host", "127.0.0.1");
	static final int PORT = Integer
			.parseInt(System.getProperty("port", "8463"));

	private NotificationCallback callback;
	private EventLoopGroup clientNettyGroup;
	private String SessionId;
	private NettyClientInboundHandler nettyClientHandler;

	/**
	 * NettyClient
	 */
	public NettyClient(NotificationCallback callback, String SessionId) {
		this.callback = callback;
		this.SessionId = SessionId;
	}

	/**
	 * Connects to netty server on host:port 
	 * 
	 * @throws Exception
	 */
	public void start() throws Exception {
		LOG.info("Starting crossant client for " + this.SessionId);

		final Bootstrap bootstrap = new Bootstrap();
		clientNettyGroup = new NioEventLoopGroup();

		nettyClientHandler = new NettyClientInboundHandler(this.callback,
				this.SessionId);

		bootstrap
				.group(clientNettyGroup)
				.channel(NioSocketChannel.class)
				.option(ChannelOption.ALLOCATOR, PooledByteBufAllocator.DEFAULT)
				.option(ChannelOption.TCP_NODELAY, true)
				.option(ChannelOption.SO_KEEPALIVE, true)
				.option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 3000)
				// ??
				.handler(new NettyClientInitializer(nettyClientHandler))
				.validate();

		// Start the client.
		ChannelFuture channelFuture = bootstrap.connect(HOST, PORT);
		final CountDownLatch channelLatch = new CountDownLatch(1);
		channelFuture.addListener(new ChannelFutureListener() {
			public void operationComplete(ChannelFuture cf) throws Exception {
				if (cf.isSuccess()) {
					channelLatch.countDown();
				} else {
					clientNettyGroup.shutdownGracefully();
					throw new Exception(
							"crossant client interrupted while waiting for streaming "
									+ "connection to arrive.");
				}
			}
		});

		try {
			channelLatch.await();
		} catch (InterruptedException ex) {
			throw new Exception(
					"crossant client interrupted while waiting for streaming "
							+ "connection to arrive.");
		}
	}

	/**
	 * Disconnects from netty server
	 */
	private void shutdownClient() {
		LOG.info("Shutting down crossant client " + this.SessionId);
		if(clientNettyGroup != null) {
			clientNettyGroup.shutdownGracefully(500, 1000, TimeUnit.MILLISECONDS);
		}
		
		// exit JVM
		System.exit(0);
	}

	/**
	 * Send signal to netty server
	 * 
	 * @param notification
	 */
	public void notifyServer(final Notification notification) {
		try {
			LOG.info("crossant client " + SessionId + " sending '" + notification.getType().toString() + "' to crossant server");
			nettyClientHandler.sendResponse(notification);
			
			if(notification.getType() == Type.CLIENT_DISCONNECTED) {
				shutdownClient();
			}
		} finally {
			if (notification.getType() == Notification.Type.CLIENT_DISCONNECTED) {
				shutdownClient();
			}
		}
	}
}
