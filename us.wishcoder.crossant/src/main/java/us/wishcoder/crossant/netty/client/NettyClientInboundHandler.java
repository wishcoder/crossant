package us.wishcoder.crossant.netty.client;

import us.wishcoder.crossant.netty.Notification;
import us.wishcoder.crossant.netty.NotificationCallback;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;


/**
 * <h1>Handles netty client/server communication </h1>
 * 
 * @author Ajay Singh
 * @version 1.0
 * @since 2016-01-01
 * 
 *
 */
public class NettyClientInboundHandler extends ChannelInboundHandlerAdapter {

	private NotificationCallback callback;
	private String SessionId;
	private Channel channel;
	
	
	/**
	 * @param callback
	 */
	public NettyClientInboundHandler(NotificationCallback callback, String SessionId) {
		this.callback = callback;
		this.SessionId = SessionId;
	}

	@Override
	public void channelActive(ChannelHandlerContext ctx) {
		callback.onConnect();
	}

	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg)
			throws Exception {
		callback.onData(msg);
	}

	@Override
	public void channelReadComplete(ChannelHandlerContext ctx) {
		ctx.flush();
	}

	@Override
	public void channelInactive(ChannelHandlerContext ctx) throws Exception {
		callback.onDisconnect();
	}

	@Override
	public void channelRegistered(ChannelHandlerContext ctx) {
		channel = ctx.channel();
	}
	
	/**
	 * Send response to connected clients
	 * @param notification
	 */
	public void sendResponse(Notification notification) {
		channel.writeAndFlush(notification);
	}
	
	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
		callback.onError("error", cause);
		ctx.close();
	}
}
