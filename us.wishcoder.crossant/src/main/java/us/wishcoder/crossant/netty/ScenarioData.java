package us.wishcoder.crossant.netty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.junit.runner.Result;

import us.wishcoder.crossant.launcher.CrossantEnvironment;
import us.wishcoder.crossant.netty.ScenarioData.Test.FailureResult;

/**
 * <h1>Holds scenario data from .xlsx file</h1>
 * 
 * @author Ajay Singh
 * @version 1.0
 * @since 2016-01-01
 * 
 *
 */
public class ScenarioData implements Serializable {
	private static final long serialVersionUID = 6269326661433819262L;

	public static enum Status {
		READY("READY"), DONE("DONE"), PENDING("PENDING");

		private String status;

		Status(String value) {
			this.status = value;
		}

		@Override
		public String toString() {
			return this.status;
		}
	}

	private final String uid;
	private final List<ScenarioSheet> scenarioSheets;

	/**
	 * @param uid
	 * @param scenarioSheets
	 */
	public ScenarioData(String uid, List<ScenarioSheet> scenarioSheets) {
		this.uid = uid;
		this.scenarioSheets = scenarioSheets;
	}

	/**
	 * @return the uid
	 */
	public final String getUid() {
		return uid;
	}

	/**
	 * @return the ScenarioSheets
	 */
	public List<ScenarioSheet> getScenarioSheets() {
		if (scenarioSheets == null) {
			return new ArrayList<ScenarioData.ScenarioSheet>();
		} else {
			return scenarioSheets;
		}
	}

	/**
	 * @return the getScenarioCount
	 */
	public final int getScenarioCount() {
		return getScenarioSheets().size();
	}

	/**
	 * @return the getTestsCount
	 */
	public final int getTestsCount() {
		int TestsCount = 0;
		for (ScenarioSheet sheet : getScenarioSheets()) {
			for (Test test : sheet.getTests()) {
				if(!CrossantEnvironment.JUNIT_REPORT_IGNORE_TEST.equalsIgnoreCase(test.getMethodName())) {
					Result result = test.getTestResult();
					if(result.getFailureCount() == 0){
						TestsCount ++;
					}
				}
			}
		}
		return TestsCount;
	}
	
	/**
	 * @param sheetName
	 * @return the getTestsCount
	 */
	public final int getTestsCount(String sheetName) {
		int testsCount = 0;
		for (ScenarioSheet sheet : getScenarioSheets()) {
			if(sheetName.equalsIgnoreCase(sheet.getName())) {
				for (Test test : sheet.getTests()) {
					if(!CrossantEnvironment.JUNIT_REPORT_IGNORE_TEST.equalsIgnoreCase(test.getMethodName())) {
						Result result = test.getTestResult();
						if(result.getFailureCount() == 0){
							testsCount ++;
						}
					}
				}
			}
		}
		return testsCount;
	}

	/**
	 * @param sheetName
	 * @return the getTestsFailureCount
	 */
	public final int getTestsFailureCount(String sheetName) {
		int testsFailureCount = 0;
		for (ScenarioSheet sheet : getScenarioSheets()) {
			if(sheetName.equalsIgnoreCase(sheet.getName())) {
				for (Test test : sheet.getTests()) {
					if(!CrossantEnvironment.JUNIT_REPORT_IGNORE_TEST.equalsIgnoreCase(test.getMethodName())) {
						Result result = test.getTestResult();
						if(result.getFailureCount() > 0){
							testsFailureCount ++;
						}else if(test.getFailures() != null && test.getFailures().size() > 0) {
							testsFailureCount ++;
						}
					}
				}
			}
		}
		return testsFailureCount;
	}
	
	/**
	 * @return the getTestsFailureCount
	 */
	public final int getTestsFailureCount() {
		int TestsFailureCount = 0;
		for (ScenarioSheet sheet : getScenarioSheets()) {
			for (Test test : sheet.getTests()) {
				if(!CrossantEnvironment.JUNIT_REPORT_IGNORE_TEST.equalsIgnoreCase(test.getMethodName())) {
					Result result = test.getTestResult();
					if(result.getFailureCount() > 0){
						TestsFailureCount ++;
					}else if(test.getFailures() != null && test.getFailures().size() > 0) {
						TestsFailureCount ++;
					}
				}
			}
		}
		return TestsFailureCount;
	}

	/**
	 * @param sheetName
	 * @return the getTestsRunTime
	 */
	public final long getTestsRunTime(String sheetName) {
		long runtime = 0;
		for (ScenarioSheet sheet : getScenarioSheets()) {
			if(sheetName.equalsIgnoreCase(sheet.getName())) {
				for (Test test : sheet.getTests()) {
					if(!CrossantEnvironment.JUNIT_REPORT_IGNORE_TEST.equalsIgnoreCase(test.getMethodName())) {
						if(test.getFailures() != null && test.getFailures().size() > 0) {
							for(FailureResult failureResult : test.getFailures()) {
								runtime += failureResult.getRunTime();
							}
						}else {
							runtime += test.getTestResult().getRunTime();
						}
					}
				}
			}
		}
		return runtime;
	}
	
	/**
	 * @return the getTestsRunTime
	 */
	public final long getTestsRunTime() {
		long runtime = 0;
		for (ScenarioSheet sheet : getScenarioSheets()) {
			for (Test test : sheet.getTests()) {
				if(!CrossantEnvironment.JUNIT_REPORT_IGNORE_TEST.equalsIgnoreCase(test.getMethodName())) {
					if(test.getFailures() != null && test.getFailures().size() > 0) {
						for(FailureResult failureResult : test.getFailures()) {
							runtime += failureResult.getRunTime();
						}
					}else {
						runtime += test.getTestResult().getRunTime();
					}
				}
			}
		}
		return runtime;
	}
	
	/**
	 * getTests
	 * @return List of Test
	 */
	public final List<Test> getTests(){
		List<Test> TestList = new ArrayList<ScenarioData.Test>();
		for (ScenarioSheet sheet : getScenarioSheets()) {
			for (Test test : sheet.getTests()) {
				TestList.add(test);
			}
		}
		return TestList;
	}
	
	
	/**
	 * Get the first test user from first scenario sheet to start tests
	 * 
	 * @return Test
	 */
	public final Test getFirstTest() {
		Test firstTest = null;

		try {
			ScenarioSheet firstScenarioSheet = getScenarioSheets().get(0);
			firstTest = firstScenarioSheet.getTests().get(0);
		} catch (Exception e) {
			// consume error
		}

		return firstTest;
	}

	/**
	 * Client will get current ready Test and set current status to
	 * Status.DONE. Test that is sent from server to execute
	 * This should be called from client before sending notification to server
	 * @return Test
	 */
	public final Test getAndUpdateReadyTest() {
		Test currentReadyTest = null;

		for (ScenarioSheet sheet : getScenarioSheets()) {
			if (sheet.getStatus() == Status.READY) {
				for (Test test : sheet.getTests()) {
					if (test.getStatus() == Status.READY) {
						test.setStatus(Status.DONE);
						currentReadyTest = test;
						break;
					}
				}
			}
			if (currentReadyTest != null) {
				break;
			}
		}

		return currentReadyTest;
	}

	/**
	 * Get current ready test
	 * @return Test
	 */
	public final Test getReadyTest() {
		Test readyTest = null;

		for (ScenarioSheet sheet : getScenarioSheets()) {
			if (sheet.getStatus() == Status.READY) {
				for (Test test : sheet.getTests()) {
					if (test.getStatus() == Status.READY) {
						readyTest = test;
						break;
					}
				}
			}
			if (readyTest != null) {
				break;
			}
		}

		return readyTest;
	}
	
	/**
	 * Update all status based on status in ScenarioSheet and Test.
	 * This should be called by server after receiving notification from client
	 */
	public final void updateScenarioSheetStatus() {
		for (ScenarioSheet sheet : getScenarioSheets()) {

			// set Status.READY for Test that should be executed next
			Test currentTest = null;
			Iterator<Test> TestIt = sheet.getTests().iterator();
			while (TestIt.hasNext()) {
				currentTest = TestIt.next();
				if (currentTest.getStatus() == Status.DONE) {
					continue;
				}

				currentTest.setStatus(Status.READY);
				break;
			}

			sheet.setStatus(currentTest.getStatus());
		}
	}

	@Override
	public ScenarioData clone() {
		List<ScenarioSheet> scenarioSheets = new ArrayList<ScenarioSheet>();
		for (ScenarioSheet sheet : getScenarioSheets()) {
			ScenarioSheet scenarioSheet = sheet.cloneFromSource(sheet
					.getStatus());
			scenarioSheets.add(scenarioSheet);
		}
		return new ScenarioData(getUid(), scenarioSheets);
	}

	@Override
	public String toString() {
		StringBuffer sBuffer = new StringBuffer();

		sBuffer.append("ScenarioData{");
		sBuffer.append("uid=" + getUid() + ", ");
		sBuffer.append("ScenarioSheets=[");
		for (ScenarioSheet ScenarioSheet : scenarioSheets) {
			sBuffer.append(ScenarioSheet.toString() + ",");
		}
		sBuffer.append("]");
		sBuffer.append("}");

		return sBuffer.toString();
	}

	// ScenarioSheet
	/**
	 * <h1>Holds data from each scenario tab in .xlsx file</h1>
	 * 
	 * @author Ajay Singh
	 * @version 1.0
	 * @since 2016-01-01
	 * 
	 *
	 */
	public static class ScenarioSheet implements Serializable{
		private static final long serialVersionUID = -7737417954875288990L;
		private final String name;
		private final String description;
		private final boolean ignore;
		private final boolean failFast;
		private final List<Test> Tests;

		private Status status;
		private boolean processed;

		/**
		 * @param name
		 * @param description
		 * @param ignore
		 * @param failFast
		 * @param tests
		 */
		public ScenarioSheet(String name, String description, boolean ignore,
				boolean failFast, List<Test> Tests) {
			this.name = name;
			this.description = description;
			this.ignore = ignore;
			this.failFast = failFast;
			this.Tests = Tests;

			this.status = Status.PENDING; // default status is pending
			assignParentToTests();
		}

		/**
		 * @param name
		 * @param description
		 * @param ignore
		 * @param failFast
		 * @param tests
		 */
		private ScenarioSheet(String name, String description, boolean ignore,
				boolean failFast, List<Test> Tests, Status status) {
			this(name, description, ignore, failFast, Tests);
			this.status = status;
		}

		/**
		 * @return the name
		 */
		public String getName() {
			return name;
		}

		/**
		 * @return the description
		 */
		public final String getDescription() {
			return description;
		}

		/**
		 * @return the ignore
		 */
		public final boolean isIgnore() {
			return ignore;
		}

		/**
		 * @return the failFast
		 */
		public final boolean isFailFast() {
			return failFast;
		}

		/**
		 * @return the Tests
		 */
		public final List<Test> getTests() {
			if (Tests == null) {
				return new ArrayList<ScenarioData.Test>();
			} else {
				return Tests;
			}
		}

		/**
		 * @return the status
		 */
		public Status getStatus() {
			return status;
		}

		/**
		 * @param status
		 *            the status to set
		 */
		public void setStatus(Status status) {
			this.status = status;
		}

		/**
		 * @return the processed
		 */
		public boolean isProcessed() {
			return processed;
		}

		/**
		 * @param processed the processed to set
		 */
		public void setProcessed(boolean processed) {
			this.processed = processed;
		}

		/**
		 * @return the TestCount
		 */
		public final int getTestCount() {
			return getTests().size();
		}

		/**
		 * Make a clone from source with updated Status
		 * 
		 * @param status
		 * @return ScenarioSheet
		 */
		public ScenarioSheet cloneFromSource(Status status) {
			List<Test> tests = new ArrayList<ScenarioData.Test>();
			for (Test test : getTests()) {
				Test Test = test.cloneFromSource(test.getStatus());
				tests.add(Test);
			}
			return new ScenarioSheet(getName(), getDescription(), isIgnore(),
					isFailFast(), tests, status);
		}

		/**
		 * Assign ScenarioSheet to all Tests
		 */
		private void assignParentToTests() {
			for (Test test : getTests()) {
				test.setParentScenarioSheet(this);
			}
		}
		
		@Override
		public String toString() {
			StringBuffer sBuffer = new StringBuffer();

			sBuffer.append("ScenarioSheet{");
			sBuffer.append("name=" + getName() + ", ");
			sBuffer.append("description=" + getDescription() + ", ");
			sBuffer.append("ignore=" + isIgnore() + ", ");
			sBuffer.append("failFast=" + isFailFast() + ", ");
			sBuffer.append("status=" + getStatus().toString() + ", ");
			sBuffer.append("Tests=[");
			for (Test Test : Tests) {
				sBuffer.append(Test.toString() + ",");
			}
			sBuffer.append("]");
			sBuffer.append("}");

			return sBuffer.toString();
		}
	}

	/**
	 * <h1>Holds test data from scenario sheet</h1>
	 * 
	 * @author Ajay Singh
	 * @version 1.0
	 * @since 2016-01-01
	 * 
	 *
	 */
	public static class Test implements Serializable {
		private static final long serialVersionUID = -6270490730923212881L;
		private final long uid;
		private final double delay;
		private final String user;
		private final String className;
		private final String methodName;
		private ScenarioSheet parentScenarioSheet;

		private Status status;
		private Result testResult;
		private List<FailureResult> failures;

		/**
		 * @param delay
		 * @param user
		 * @param className
		 * @param methodName
		 * @param parentScenarioSheet
		 */
		public Test(double delay, String user, String className,
				String methodName, ScenarioSheet parentScenarioSheet) {
			this.uid = System.currentTimeMillis();
			this.delay = delay;
			this.user = user;
			this.className = className;
			this.methodName = methodName;
			this.parentScenarioSheet = parentScenarioSheet;

			this.status = Status.PENDING; // default status is pending
			this.testResult = new Result(); // default empty result
			this.failures = new ArrayList<ScenarioData.Test.FailureResult>(); // default empty failures
		}

		/**
		 * @param delay
		 * @param user
		 * @param className
		 * @param methodName
		 * @param parentScenarioSheet
		 * @param status
		 * @param testResult
		 */
		private Test(double delay, String user, String className,
				String methodName, ScenarioSheet parentScenarioSheet,
				Status status, Result testResult) {
			this(delay, user, className, methodName, parentScenarioSheet);
			this.status = status;
			this.testResult = testResult;
			this.failures = new ArrayList<ScenarioData.Test.FailureResult>(); // default empty failures
		}

		/**
		 * @return the uid
		 */
		public final long getUid() {
			return uid;
		}

		/**
		 * @return the delay
		 */
		public double getDelay() {
			return delay;
		}

		/**
		 * @return the user
		 */
		public String getUser() {
			return user;
		}

		/**
		 * @return the className
		 */
		public String getClassName() {
			return className;
		}

		/**
		 * @return the methodName
		 */
		public String getMethodName() {
			return methodName;
		}

		/**
		 * @param parentScenarioSheet the parentScenarioSheet to set
		 */
		public void setParentScenarioSheet(ScenarioSheet parentScenarioSheet) {
			this.parentScenarioSheet = parentScenarioSheet;
		}

		/**
		 * @return the parentScenarioSheet
		 */
		public ScenarioSheet getParentScenarioSheet() {
			return parentScenarioSheet;
		}

		/**
		 * @return the status
		 */
		public Status getStatus() {
			return status;
		}

		/**
		 * @param status
		 *            the status to set
		 */
		public void setStatus(Status status) {
			this.status = status;
		}

		/**
		 * @return the testResult
		 */
		public Result getTestResult() {
			return testResult;
		}

		/**
		 * @param testResult
		 *            the testResult to set
		 */
		public void setTestResult(Result testResult) {
			this.testResult = testResult;
		}

		/**
		 * Clone from source using the updated Status
		 * 
		 * @param status
		 * @return Test
		 */
		public Test cloneFromSource(Status status) {
			return new Test(getUid(), getUser(), getClassName(),
					getMethodName(), getParentScenarioSheet(), status,
					getTestResult());
		}
		
		/**
		 * @return the failures
		 */
		public List<FailureResult> getFailures() {
			return failures;
		}

		/**
		 * @param failures the failures to set
		 */
		public void setFailures(List<FailureResult> failures) {
			this.failures = failures;
		}

		@Override
		public String toString() {
			StringBuffer sBuffer = new StringBuffer();

			sBuffer.append("Test{");
			sBuffer.append("uid=" + getUid() + ", ");
			sBuffer.append("delay=" + getDelay() + ", ");
			sBuffer.append("user=" + getUser() + ", ");
			sBuffer.append("className=" + getClassName() + ", ");
			sBuffer.append("methodName=" + getMethodName() + ", ");
			sBuffer.append("status=" + getStatus().toString() + ", ");
			sBuffer.append("testResult=" + getTestResult().wasSuccessful());
			sBuffer.append("}");

			return sBuffer.toString();
		}
		
		public static class FailureResult implements Serializable{
			private static final long serialVersionUID = -5698790461285935806L;
			
			private final String description;
			private final String message;
			private final String trace;
			private final long runTime;
			
			/**
			 * 
			 * @param description
			 * @param message
			 * @param trace
			 * @param runTime;
			 */
			public FailureResult(String description, String message, String trace, long runTime) {
				super();
				this.description = description;
				this.message = message;
				this.trace = trace;
				this.runTime = runTime;
			}

			public String getDescription() {
				return description;
			}

			public String getMessage() {
				return message;
			}

			public String getTrace() {
				return trace;
			}

			/**
			 * @return the runTime
			 */
			public long getRunTime() {
				return runTime;
			}

			@Override
			public String toString() {
				return "FailureResult [description=" + description + ", message=" + message + ", trace=" + trace
						+ ", runTime=" + runTime + "]";
			}

		}
	}
}
