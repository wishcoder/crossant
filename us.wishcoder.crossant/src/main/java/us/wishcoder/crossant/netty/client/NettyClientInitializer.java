package us.wishcoder.crossant.netty.client;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.serialization.ClassResolvers;
import io.netty.handler.codec.serialization.ObjectDecoder;
import io.netty.handler.codec.serialization.ObjectEncoder;

/**
 * <h1>Initialize {@link NettyClientInboundHandler} for {@link NettyClient}</h1>
 *
 * @author Ajay Singh
 * @version 1.0
 * @since 2016-01-01
 * 
 *
 */
public class NettyClientInitializer extends ChannelInitializer<SocketChannel> {

	private NettyClientInboundHandler nettyClientHandler;

	/**
	 * Constructor
	 * 
	 * @param callback
	 */
	public NettyClientInitializer(NettyClientInboundHandler nettyClientHandler) {
		this.nettyClientHandler = nettyClientHandler;
	}

	/**
	 * Initialize ChannelPipeline for {@link NettyClient}
	 * 
	 * @param ch  SocketChannel  
	 */
	@Override
	protected void initChannel(SocketChannel ch) throws Exception {
		final ChannelPipeline pipeline = ch.pipeline();
		//pipeline.addLast("logger", new LoggingHandler(LogLevel.INFO));
		pipeline.addLast("encoder", new ObjectEncoder());
		pipeline.addLast("decoder",
				new ObjectDecoder(ClassResolvers.cacheDisabled(null)));
		pipeline.addLast("responseHandler", this.nettyClientHandler);
	}

}
