package us.wishcoder.crossant.netty.server;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

import java.util.HashMap;
import java.util.Map;

import us.wishcoder.crossant.netty.Notification;
import us.wishcoder.crossant.netty.NotificationCallback;


/**
 * <h1>Handles netty server/client communication </h1>
 * 
 * @author Ajay Singh
 * @version 1.0
 * @since 2016-01-01
 * 
 *
 */
@Sharable
public class NettyServerInboundHandler extends ChannelInboundHandlerAdapter {
	private NotificationCallback callback;
	private int UnitTestCount;
	private Map<String, Channel> channelMap;
	
	
	/**
	 * @param callback
	 */
	public NettyServerInboundHandler(NotificationCallback callback, int UnitTestCount) {
		this.callback = callback;
		this.UnitTestCount = UnitTestCount;
		this.channelMap = new HashMap<String, Channel>();
	}

	@Override
	public void channelActive(ChannelHandlerContext ctx) {
		callback.onConnect();
	}

    @Override
    public void channelRead(
            ChannelHandlerContext ctx, Object msg) throws Exception {
    	
    	if(this.channelMap.size() < this.UnitTestCount){
    		Notification notification = (Notification) msg;
    		this.channelMap.put(notification.getSessionId(), ctx.channel());
    	}
    	
    	callback.onData(msg);
    }

	@Override
	public void channelReadComplete(ChannelHandlerContext ctx) {
		ctx.flush();
	}

	@Override
	public void channelRegistered(ChannelHandlerContext ctx) {
	}
	
	@Override
	public void channelInactive(ChannelHandlerContext ctx) throws Exception {
		callback.onDisconnect();
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
		callback.onError("error", cause);
		ctx.close();
	}
	
	/**
	 * Send response to connected clients
	 * @param notification
	 */
	public void sendResponse(Notification notification) {
		this.channelMap.get(notification.getSessionId()).writeAndFlush(notification);
	}
}
