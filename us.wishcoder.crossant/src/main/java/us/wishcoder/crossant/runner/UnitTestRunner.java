package us.wishcoder.crossant.runner;

import java.util.List;

import org.junit.internal.runners.statements.Fail;
import org.junit.runner.Description;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.InitializationError;
import org.junit.runners.model.Statement;

/**
 * <h1>Used to run JUnit test cases</h1>
 * 
 * @author Ajay Singh
 * @version 1.0
 * @since 2016-01-01
 * 
 *
 */
public class UnitTestRunner extends BlockJUnit4ClassRunner {
	private String method;
	private String runnerName;

	/**
	 * @param klass
	 * @throws InitializationError
	 */
	public UnitTestRunner(String runnerName, Class<?> klass, String method)
			throws InitializationError {
		super(klass);

		this.runnerName = runnerName;
		this.method = method;
	}

	@Override
	protected void runChild(final FrameworkMethod method, RunNotifier notifier) {
		Description description = describeChild(method);
		if(description.getMethodName().equalsIgnoreCase(this.method)){
			Statement statement;
			try {
				statement = methodBlock(method);
			} catch (Throwable ex) {
				statement = new Fail(ex);
			}
			runLeaf(statement, description, notifier);
		}
	}

	@Override
	public Description getDescription() {
		Description desc = Description.createSuiteDescription(this.runnerName);
		desc.addChild(Description.createTestDescription(super.getTestClass()
				.getJavaClass(), this.method));
		return desc;
	}
}
