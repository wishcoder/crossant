# Crossant#

### What is this repository for? ###
Crossant (Cross JVM Scenario Testing Using ANT API and JUnit) is an open source java library published under [LGPLv3.0](http://www.gnu.org/licenses/lgpl-3.0.html) license.

* Version: 1.0

### How do I get set up? ###

* Check out repository in Eclipse as a Java/Maven project  
* View [Crossant](https://bitbucket.org/wishcoder/crossant/wiki/Home) wiki for more details

### Who do I talk to? ###
Ajay Singh [message4ajay@gmail.com]